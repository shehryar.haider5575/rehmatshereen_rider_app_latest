import 'package:flutter/material.dart';
import 'package:rehmatshereen_rider_app/Common/Theme.dart';

AppBar appBarSimple(
    {@required String title, Function onTap, Function onRefresh}) {
  return AppBar(
    elevation: 0,
    centerTitle: true,
    leading: InkWell(
      onTap: onTap,
      child: Padding(
        padding: const EdgeInsets.only(left: 10.0),
        child: Icon(
          Icons.menu,
          color: Colors.white,
          size: 30,
        ),
      ),
    ),
    title: Text(
      title,
      style: TextStyle(color: Colors.white),
    ),
    actions: [
      InkWell(
          onTap: onRefresh,
          child: Padding(
            padding: const EdgeInsets.only(right: 25.0),
            child: Icon(
              Icons.refresh,
              color: Colors.white,
            ),
          )),
    ],
    backgroundColor: appTheme.primaryColor,
    // leading: BackButton(
    //   color: Colors.white,
    // ),
  );
}
