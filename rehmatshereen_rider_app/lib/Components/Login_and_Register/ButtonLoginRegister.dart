import 'package:flutter/material.dart';
import 'package:rehmatshereen_rider_app/Common/Theme.dart';

class ButtonLoginRegister extends StatelessWidget {
  const ButtonLoginRegister(
      {this.onPressed, this.buttonText, this.padding, this.buttonColor});

  final onPressed;
  final String buttonText;
  final EdgeInsets padding;
  final Color buttonColor;

  @override
  Widget build(BuildContext context) {
    return FlatButton(
        textColor: appTheme.primaryColor,
        padding: padding ?? EdgeInsets.symmetric(vertical: 5.0),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
        color: buttonColor,
        onPressed: onPressed,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          child: Center(
            child: Text(
              buttonText,
              style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w400),
            ),
          ),
        ));
  }
}
