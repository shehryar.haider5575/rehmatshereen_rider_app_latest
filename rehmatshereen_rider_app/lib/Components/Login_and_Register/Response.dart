import 'package:flutter/material.dart';

class ResponseStatus extends StatefulWidget {
      final String responseText;

  const ResponseStatus({this.responseText});
  @override
  _ResponseStatusState createState() => _ResponseStatusState();
}

class _ResponseStatusState extends State<ResponseStatus> {
  @override
  Widget build(BuildContext context) {

 return AlertDialog(
          title: Padding(
            padding: const EdgeInsets.only(left: 20.0),
            child: Text("Terms & Conditions"),
          ),

          content: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 5.0),
                child: Icon(
                  Icons.check_box,
                  color: Colors.orange[800],
                  size: 22.0,
                ),
              ),
           Text(widget.responseText),
            ],
          ),
          //   Text("I acknowledge and understand\nclient to provide content"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            // new FlatButton(
            //  child: new Text("Close"),
            //   onPressed: () {
            //     Navigator.of(context).pop();
            //   },
            // ),
          ],
        );

  }
}
