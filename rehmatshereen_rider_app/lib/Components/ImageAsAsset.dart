import 'package:flutter/material.dart';

class ImageAsAsset extends StatelessWidget {
  const ImageAsAsset(
      {this.image, this.height, this.scale, this.fit, this.width});

  final String image;
  final double height;
  final double width;
  final double scale;
  final BoxFit fit;

  @override
  Widget build(BuildContext context) {
    return
        // NetworkImage("https://picsum.photos/250?image=9");
//     Image.network(
//   'https://picsum.photos/250?image=9',
// );
        //     Image.asset(
        //   'Assets/Images/$image',
        //   height: height,
        //   width: width,
        //   scale: scale,
        //   fit: fit,
        // );

        Image.asset(
      'Assets/Images/$image',
      height: height,
      width: width,
      scale: scale,
      fit: fit,
    );
  }
}
