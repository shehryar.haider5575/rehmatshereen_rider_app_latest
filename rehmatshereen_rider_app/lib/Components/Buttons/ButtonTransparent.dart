import 'package:flutter/material.dart';

class ButtonTransparent extends StatelessWidget {
  const ButtonTransparent({
    this.onPressed,
    this.buttonText,
    this.child,
    this.textStyle,
    this.fillColor,
    this.rounded,
    this.color,
    this.padding
  });

  final onPressed;
  final String buttonText;
  final Widget child;
  final TextStyle textStyle;
  final Color fillColor;
  final double rounded;
  final Color color;
  final EdgeInsets padding;

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      padding: padding ?? EdgeInsets.all(0.0),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(rounded ?? 0.0),),
      splashColor: fillColor,
      color: color ?? null,
      onPressed: onPressed,
      child: child ?? Text(buttonText, style: textStyle,),
    );
  }
}
