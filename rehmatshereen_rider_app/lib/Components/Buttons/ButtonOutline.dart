import 'package:flutter/material.dart';

class ButtonOutline extends StatelessWidget {
  const ButtonOutline({
    @required this.buttonText,
    this.borderColor,
    this.radius,
    this.textColor,
    this.onPressed
  });

  final String buttonText;
  final double radius;
  final Color textColor;
  final Color borderColor;
  final onPressed;

  @override
  Widget build(BuildContext context) {
    return OutlineButton(
      onPressed: onPressed,
      child: Text(buttonText),
      textColor: textColor ?? Colors.white,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(radius ?? 15.0)),
      borderSide: BorderSide(color: borderColor ?? Colors.white),
    );
  }
}