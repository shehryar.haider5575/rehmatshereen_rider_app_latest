import 'package:flutter/material.dart';

ThemeData appTheme = ThemeData.dark().copyWith(
//  primarySwatch: Colors.blue,
//    primaryColor: Colors.grey.shade400,
    accentColor: Color(0xFFfde31e));

const kCollectionHeading = TextStyle(fontSize: 20, fontWeight: FontWeight.bold);

const kTextOnImageCollection = TextStyle(
  fontSize: 20,
  fontWeight: FontWeight.bold,
  color: Colors.white,
);

const kSwiperText =
    TextStyle(fontWeight: FontWeight.bold, fontSize: 32, color: Colors.white);

const kCollectionItemName = TextStyle(
  fontSize: 14.0,
  fontWeight: FontWeight.bold,
);

const kCollectionItemPrice = TextStyle(fontSize: 12.0, color: Colors.grey);

const kCollectionRatingText =
    TextStyle(fontSize: 11.0, fontWeight: FontWeight.bold);

const kCollectionPillText = TextStyle(
  fontSize: 10.0,
  color: Colors.white,
);

const kProfileTabs = BoxDecoration(
  borderRadius: BorderRadius.all(Radius.circular(10.0)),
  boxShadow: [
    BoxShadow(
      color: Colors.black12,
      blurRadius: 5.0,
      spreadRadius: 0.0,
      offset: Offset(0.0, 2.0),
    ),
  ],
);
