import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get_storage/get_storage.dart';
import 'package:rehmatshereen_rider_app/screens/splash_screen.dart';

import 'Common/Theme.dart';
import 'screens/Login_screen.dart';
import 'screens/Orders_screen.dart';

void main() async {
  await GetStorage.init();
  runApp(MyApp());
}

// 450308

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Rehmat Shereen RriderApp',
      theme: appTheme,
      initialRoute: '/',
      routes: {
        '/login': (context) => Login(),
        '/OrdersScreen': (context) => OrdersScreen(),
        // '/MapPage': (context) => MapPageScreen(),
      },
      // home: UserTaggin(),
      home: SplashScreen(),
    );
  }
}
