import 'package:flutter/material.dart';

class MyNavigator {
  static void goToLoginScreen(BuildContext context) {
    Navigator.pushNamed(context, '/login');
  }

  static void goToBottomNavBar(BuildContext context) {
    Navigator.pushNamed(context, '/OrdersScreen');
  }
}
