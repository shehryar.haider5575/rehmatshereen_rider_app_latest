class RiderDetails {
  Success success;

  RiderDetails({this.success});

  RiderDetails.fromJson(Map<String, dynamic> json) {
    success =
        json['success'] != null ? new Success.fromJson(json['success']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.success != null) {
      data['success'] = this.success.toJson();
    }
    return data;
  }
}

class Success {
  int id;
  int riderId;
  String name;
  String contact;
  String status;
  String deletedAt;
  String createdAt;
  String updatedAt;
  String deviceToken;
  String dutyTimings;
  String branch;

  Success(
      {this.id,
      this.riderId,
      this.name,
      this.contact,
      this.status,
      this.deletedAt,
      this.createdAt,
      this.updatedAt,
      this.deviceToken,
      this.dutyTimings,
      this.branch});

  Success.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    riderId = json['rider_id'];
    name = json['name'];
    contact = json['contact'];
    status = json['status'];
    deletedAt = json['deleted_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deviceToken = json['device_token'];
    dutyTimings = json['duty_timings'];
    branch = json['branch'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['rider_id'] = this.riderId;
    data['name'] = this.name;
    data['contact'] = this.contact;
    data['status'] = this.status;
    data['deleted_at'] = this.deletedAt;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['device_token'] = this.deviceToken;
    data['duty_timings'] = this.dutyTimings;
    data['branch'] = this.branch;
    return data;
  }
}
