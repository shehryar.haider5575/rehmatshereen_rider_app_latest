class RiderAssignedOrders {
  List<Data> data;
  int code;
  String message;
  String imageUrl;

  RiderAssignedOrders({this.data, this.code, this.message, this.imageUrl});

  RiderAssignedOrders.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
    code = json['code'];
    message = json['message'];
    imageUrl = json['image_url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    data['code'] = this.code;
    data['message'] = this.message;
    data['image_url'] = this.imageUrl;
    return data;
  }
}

class Data {
  int id;
  int riderId;
  String technosysOrderNo;
  int isCollect;
  String deliveryStatus;
  String deletedAt;
  String createdAt;
  String updatedAt;
  Order order;

  Data(
      {this.id,
      this.riderId,
      this.technosysOrderNo,
      this.isCollect,
      this.deliveryStatus,
      this.deletedAt,
      this.createdAt,
      this.updatedAt,
      this.order});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    riderId = json['rider_id'];
    technosysOrderNo = json['technosys_order_no'];
    isCollect = json['is_collect'];
    deliveryStatus = json['delivery_status'];
    deletedAt = json['deleted_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    order = json['order'] != null ? new Order.fromJson(json['order']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['rider_id'] = this.riderId;
    data['technosys_order_no'] = this.technosysOrderNo;
    data['is_collect'] = this.isCollect;
    data['delivery_status'] = this.deliveryStatus;
    data['deleted_at'] = this.deletedAt;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.order != null) {
      data['order'] = this.order.toJson();
    }
    return data;
  }
}

class Order {
  int id;
  String orderNo;
  String technosysOrderNo;
  int userId;
  String delieveryCharges;
  String totalAmount;
  String phone;
  String address;
  int status;
  String date;
  String deletedAt;
  String createdAt;
  String updatedAt;
  dynamic orderDeliverTypeId;
  dynamic paymentModeId;
  dynamic addressLat;
  dynamic addressLong;

  Order(
      {this.id,
      this.orderNo,
      this.technosysOrderNo,
      this.userId,
      this.delieveryCharges,
      this.totalAmount,
      this.phone,
      this.address,
      this.status,
      this.date,
      this.deletedAt,
      this.createdAt,
      this.updatedAt,
      this.orderDeliverTypeId,
      this.paymentModeId,
      this.addressLat,
      this.addressLong});

  Order.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    orderNo = json['order_no'];
    technosysOrderNo = json['technosys_order_no'];
    userId = json['user_id'];
    delieveryCharges = json['delievery_charges'];
    totalAmount = json['total_amount'];
    phone = json['phone'];
    address = json['address'];
    status = json['status'];
    date = json['date'];
    deletedAt = json['deleted_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    orderDeliverTypeId = json['order_deliver_type_id'];
    paymentModeId = json['payment_mode_id'];
    addressLat = json['address_lat'];
    addressLong = json['address_long'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['order_no'] = this.orderNo;
    data['technosys_order_no'] = this.technosysOrderNo;
    data['user_id'] = this.userId;
    data['delievery_charges'] = this.delieveryCharges;
    data['total_amount'] = this.totalAmount;
    data['phone'] = this.phone;
    data['address'] = this.address;
    data['status'] = this.status;
    data['date'] = this.date;
    data['deleted_at'] = this.deletedAt;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['order_deliver_type_id'] = this.orderDeliverTypeId;
    data['payment_mode_id'] = this.paymentModeId;
    data['address_lat'] = this.addressLat;
    data['address_long'] = this.addressLong;
    return data;
  }
}
