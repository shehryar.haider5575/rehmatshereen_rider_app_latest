class AssignedOrderStatusUpdated {
  String data;
  int code;
  String message;
  String imageUrl;

  AssignedOrderStatusUpdated(
      {this.data, this.code, this.message, this.imageUrl});

  AssignedOrderStatusUpdated.fromJson(Map<String, dynamic> json) {
    data = json['data'];
    code = json['code'];
    message = json['message'];
    imageUrl = json['image_url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['data'] = this.data;
    data['code'] = this.code;
    data['message'] = this.message;
    data['image_url'] = this.imageUrl;
    return data;
  }

  //* --- getters ---

  int get getCode => code;
}
