import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get_storage/get_storage.dart';
// import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:localstorage/localstorage.dart';
import 'package:location/location.dart' as locationPackage;
import 'package:rehmatshereen_rider_app/Common/BaseAlertDialog.dart';
import 'package:rehmatshereen_rider_app/Common/Theme.dart';
import 'package:rehmatshereen_rider_app/Components/Login_and_Register/ButtonLoginRegister.dart';
import 'package:rehmatshereen_rider_app/model/model_AssignedOrderStatusDelivered.dart';
import 'package:rehmatshereen_rider_app/model/model_AssignedOrderStatusInProcess.dart';
import 'package:rehmatshereen_rider_app/model/model_AssignedOrderStatusOnWay.dart';
import 'package:rehmatshereen_rider_app/model/model_AssignedOrderStatusUpdated.dart';
import 'package:rehmatshereen_rider_app/screens/orders_screen.dart';
import 'package:rehmatshereen_rider_app/utils/baseurl.dart';
import 'package:shared_preferences/shared_preferences.dart';

const double CAMERA_ZOOM = 12.0;
const double CAMERA_TILT = 80;
const double CAMERA_BEARING = 30;
const LatLng SOURCE_LOCATION = LatLng(24.860333, 67.064573);
const LatLng DEST_LOCATION = LatLng(24.843776, 67.055906);

class TravelToDeliveryAddress extends StatefulWidget {
//  final LatLng comingSourceLocation;
  final LatLng comingDestinationLocation;
  TravelToDeliveryAddress(
    this.comingDestinationLocation,
  );
  @override
  _TravelToDeliveryAddressState createState() =>
      _TravelToDeliveryAddressState();
}

class _TravelToDeliveryAddressState extends State<TravelToDeliveryAddress> {
  double addressLat, addressLong;
  Set<Marker> _markers = Set<Marker>();
  Set<Polyline> _polyLines = Set<Polyline>();
  double distanceInMeters;
  double estimatedDriveTimeInMinutes;
  // CameraPosition initialCameraPosition = CameraPosition(
  //     zoom: CAMERA_ZOOM,
  //     // tilt: CAMERA_TILT,
  //     // bearing: CAMERA_BEARING,
  //     target: widget.comingSourceLocation);

  // Future<AssignedOrderStatusUpdated> _futureAssignedOrderStatusUpdated;
  // Future<AssignedOrderStatusInProcess> _futureFetchAssignedOrderStatusInProcess;
  // Future<AssignedOrderStatusOnWay> _futureFetchAssignedOrderStatusOnWay;
  // Future<AssignedOrderStatusDelivered> _futureFetchAssignedOrderStatusDelivered;

//!-------
  PolylinePoints polylinePoints = PolylinePoints();
  String googleAPiKey = "AIzaSyCVEw8VKyy9ustmvw85CFrtg5BgJm1_R1U";
  // String googleAPiKey = "AIzaSyB8LPyTTDYROup1C5Y9S7jXsAM-UKWJdyc";
  List<LatLng> polylineCoordinates = [];
  getPolyline(addLat, addlon) async {
    print('----------------((((())))))_--Address');
    print(addressLat);
    print(addressLong);
    print('----------------((((())))))_--Address');
    PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
      googleAPiKey,
      PointLatLng(addLat, addlon),
      // PointLatLng(SOURCE_LOCATION.latitude,SOURCE_LOCATION.longitude),
      PointLatLng(widget.comingDestinationLocation.latitude,
          widget.comingDestinationLocation.longitude),
      travelMode: TravelMode.driving,
    );
    if (result.points.isNotEmpty) {
      print('-------------itsresult');
      setState(() {
        result.points.forEach((PointLatLng point) {
          polylineCoordinates.add(LatLng(point.latitude, point.longitude));
        });
      });
    } else {
      print('-------------its else result');
    }
    addPolyLine();
  }

  addPolyLine() {
    print('Adding======polyline');
    print(polylineCoordinates[0].latitude);
    setState(() {
      _polyLines.add(Polyline(
        polylineId: PolylineId('id01'),
        color: Colors.red,
        width: 4,
        points: polylineCoordinates,
      ));
    });
  }

//!-----

  // SharedPreferences _prefs;
  final getStorage = GetStorage();
  String orderStatus;

  String token, riderId, orderNo;
  String technosisOrderNumber;

  bool isOrderDelivered = false;
  final LocalStorage storage = LocalStorage('token');

  Completer<GoogleMapController> _controller = Completer();

  // Position currentLocation;
  BitmapDescriptor sourceIcon;
  BitmapDescriptor destinationIcon;
  bool isLoaderVisible = false;

  //* --- overrides ---

  getDeliveryStatus() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    // prefs.getString('orderDeliveredStatus');
    orderStatus = prefs.getString('orderDeliveredStatus');
    print('this is order status $orderStatus');
    technosisOrderNumber = prefs.getString('technosisOrderNumber');
    if (orderStatus == 'delivered') {
      isOrderDelivered = true;
    } else {
      isOrderDelivered = false;
    }
    print('this is order delivered 123 $isOrderDelivered');
    return orderStatus;
  }

  getLocation() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    addressLat = prefs.getDouble('addressLat');
    addressLong = prefs.getDouble('addressLong');

    print('****************************');
  }

  locationPackage.Location location;
  locationPackage.LocationData currentLocation;

  listenLocationChanges() async {
    location = new locationPackage.Location();
    await location.changeSettings(interval: 3000, distanceFilter: 10.0);
    location.onLocationChanged().distinct().listen(
        (locationPackage.LocationData cLoc) {
      print(
          '-----------__________---------_______---------______------------__--');
      print(cLoc.latitude);
      print(cLoc.longitude);
      if(currentLocation.latitude != null ){
        if (currentLocation.latitude == cLoc.latitude &&
          currentLocation.longitude == cLoc.longitude) {
        print('Same Lati Longi');
      } else {
        currentLocation = cLoc;
        print('Not Same Lati Longi');
        updatePinOnMap();
        getDistanceInMeter(widget.comingDestinationLocation,
            LatLng(cLoc.latitude, cLoc.longitude));
      }
      }
      
     
    }, cancelOnError: true);
  }

  @override
  void initState() {
    super.initState();
    //!---

    //!---
    getLocation();
    setSourceAndDestinationIcons();
    getUserCurrentLocation().then((value) {
      addMarkers();
      getPolyline(value.latitude, value.longitude);
      getDistanceInMeter(widget.comingDestinationLocation,
          LatLng(value.latitude, value.longitude));
      listenLocationChanges();
    });
    

    // if (_markers.isEmpty) {
    //   setState(() {
    //     isLoaderVisible = true;
    //   });
    // }
    getDeliveryStatus();
    print('this is order delivery status ${getDeliveryStatus()}');
    token = storage.getItem("token").toString();
    riderId = storage.getItem("rider_id").toString();

    orderNo = storage.getItem("technosys_order_no");
    print('THIS IS TECHNOSIS ORDER NUMBER $orderNo');
    print('********');
    print('********');
    print('********');
    print('********');
    print('THIS IS LAT *************** ');
  }

  //* --- methods ---

  void updatePinOnMap() async {
    // create a new CameraPosition instance
    // every time the location changes, so the camera
    // follows the pin as it moves with an animation
    CameraPosition cPosition = CameraPosition(
      zoom: 14,
      target: LatLng(currentLocation.latitude, currentLocation.longitude),
    );
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(cPosition));
    // do this inside the setState() so Flutter gets notified
    // that a widget update is due
    setState(() {
      // updated position
      var pinPosition =
          LatLng(currentLocation.latitude, currentLocation.longitude);

      // the trick is to remove the marker (by id)
      // and add it again at the updated location
      polylineCoordinates.clear();
      getPolyline(currentLocation.latitude, currentLocation.longitude);
      // getPolyline(currentLocation.latitude,currentLocation.longitude);
      _markers.removeWhere((m) => m.markerId.value == 'rider current location');
      _markers.add(Marker(
          markerId: MarkerId('rider current location'),
          position: pinPosition, // updated position
          icon: destinationIcon));
    });
  }

  getDistanceInMeter(LatLng location1Latlong, LatLng location2Latlong) {
    distanceInMeters = Geolocator.distanceBetween(
        location1Latlong.latitude,
        location1Latlong.longitude,
        location2Latlong.latitude,
        location2Latlong.longitude);
    int speedIs10MetersPerMinute = 60;
    setState(() {
      estimatedDriveTimeInMinutes = distanceInMeters / speedIs10MetersPerMinute;
    });

    print('Its Drive Time---------');
    print(estimatedDriveTimeInMinutes);
  }

  void setSourceAndDestinationIcons() async {
    BitmapDescriptor.fromAssetImage(ImageConfiguration(devicePixelRatio: 2.0),
            'Assets/Images/destination_map_marker.png')
        .then((onValue) {
      sourceIcon = onValue;
    });

    BitmapDescriptor.fromAssetImage(ImageConfiguration(devicePixelRatio: 2.0),
            'Assets/Images/driving_pin.png')
        .then((onValue) {
      destinationIcon = onValue;
    });
  }

  Future<Position> getUserCurrentLocation() async {
    location = new locationPackage.Location();
    bool serviceEnabled;
    LocationPermission permission;
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error('Location permissions are denied');
      }
    }
   if(permission == LocationPermission.whileInUse || permission == LocationPermission.always ){
     currentLocation = await location.getLocation();
   }
    
    print('this is current location $currentLocation');
    return await Geolocator.getCurrentPosition();
  }

  addMarkers() async {
    _markers.clear();
    setState(() {
      _markers.add(
        Marker(
          markerId: MarkerId('rider current location'),
          icon: destinationIcon,
          infoWindow: InfoWindow(title: 'Your location'),
          position: LatLng(currentLocation.latitude, currentLocation.longitude),
        ),
      );
      _markers.add(Marker(
        markerId: MarkerId('rider destination'),
        infoWindow: InfoWindow(title: 'This is delivery address'),
        position: widget.comingDestinationLocation,
        // position: LatLng(addressLat, addressLong)
      ));
    });
    // if (addressLat != null && addressLong != null) {
    setState(() {
      isLoaderVisible = false;
    });
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(
      CameraUpdate.newCameraPosition(
        CameraPosition(
            target: LatLng(currentLocation.latitude, currentLocation.longitude),
            zoom: 14),
      ),
    );

    // }
  }

  bool inProcessButton = true;
  loadInProcessButtonButton(bool value) {
    if (inProcessButton == value) {
      setState(() {
        inProcessButton = value;
      });
    } else {
      setState(() {
        inProcessButton = value;
      });
    }
  }

  bool onWayButtonState = true;
  loadOnWayButtonState(bool value) {
    if (onWayButtonState == value) {
      setState(() {
        onWayButtonState = value;
      });
    } else {
      setState(() {
        onWayButtonState = value;
      });
    }
  }

  //--------------------------fetchAssignedOrderStatusUpdated---------------------
  Future<AssignedOrderStatusUpdated> fetchAssignedOrderStatusUpdated(
    String token,
    String riderId,
    String orderNo,
    String status,
  ) async {
    final http.Response response = await http.post(
      Uri.parse(BaseUrl().r_url + "rider/order-collect/status"),
      headers: <String, String>{
        // 'Accept': 'application/json',
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json'
      },
      body: jsonEncode(<String, String>{
        'rider_id': riderId,
        'orderNo': orderNo,
        'status': status,
      }),
    );

    setState(() {
      isLoaderVisible = false;
    });

    if (response.statusCode == 200) {
      print(response.statusCode.toString());
      print(response.body.toString());
      _confirmPayment('status updated successfully');

      return AssignedOrderStatusUpdated.fromJson(jsonDecode(response.body));
    } else {
      print('this is status code ${response.statusCode.toString()}');
      print(response.body.toString());
      throw Exception('Failed to Assigned Order Status Updated');
    }
  }
//--------------------------fetchAssignedOrderStatus_IN-process-----------------

  Future<AssignedOrderStatusInProcess> fetchAssignedOrderStatusInProcess(
    String token,
    String riderId,
    String orderNo,
    String status,
  ) async {
    final http.Response response = await http.post(
      Uri.parse(BaseUrl().r_url + "rider/order/delivery-status"),
      headers: <String, String>{
        // 'Accept': 'application/json',
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json'
      },
      body: jsonEncode(<String, String>{
        'rider_id': riderId,
        'orderNo': orderNo,
        'status': status,
      }),
    );

    if (response.statusCode == 200) {
      print(response.statusCode.toString());
      print(response.body.toString());
      _showDialog('Delivery InProcess', false);
      loadInProcessButtonButton(false);
      return AssignedOrderStatusInProcess.fromJson(jsonDecode(response.body));
    } else {
      print(response.statusCode.toString());
      print(response.body.toString());
      throw Exception('Failed to Assigned Order Status In Process');
    }
  }

  void _showDialog(String contentString, bool value) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        if (value == false) {
          print(value);
          Future.delayed(Duration(seconds: 1), () {
            Navigator.of(context).pop(true);
          });
        } else {}

//            Center()

//        Future.delayed(Duration(seconds: 1), () {
//                Navigator.of(context).pop(true);
//              });
        // return object of type Dialog
        return AlertDialog(
          elevation: 5,
          // title: Text(
          //   "$titlestring",
          //   // "Sign Up Successfully",
          //   textAlign: TextAlign.center,
          //    style: TextStyle(
          //         color: Colors.orange[700],
          //       ),
          // ),
          content: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              // Padding(
              //   padding: const EdgeInsets.only(right: 3),
              //   child: CircleAvatar(
              //     radius: 15,
              //     backgroundColor: Colors.orange[700],
              //     child: SizedBox(
              //       child: Icon(
              //         Icons.login_rounded,
              //         //  size: 25,
              //         color: Colors.white,
              //         // color: Colors.orange[700],
              //       ),
              //     ),
              //   ),
              // ),

              Expanded(
//                flex: 2,

                child: Text(
                  "$contentString",
                  textAlign: TextAlign.left,
                  maxLines: 4,
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),

                  // "Sign Up Successfully",
                ),
              )
            ],
          ),
        );
      },
    );
  }

  //--------------------------fetchAssignedOrderStatus_on-way-------------------

  Future<AssignedOrderStatusOnWay> fetchAssignedOrderStatusOnWay(
    String token,
    String riderId,
    String orderNo,
    String status,
  ) async {
    print('THIS IS FETCH ASSIGNED ORDER STATUS ON WAY METHOD');
    final http.Response response = await http.post(
      Uri.parse(BaseUrl().r_url + "rider/order/delivery-status"),
      headers: <String, String>{
        // 'Accept': 'application/json',
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json'
      },
      body: jsonEncode(<String, String>{
        'rider_id': riderId,
        'orderNo': orderNo,
        'status': status,
      }),
    );

    setState(() {
      isLoaderVisible = false;
    });

    if (response.statusCode == 200) {
      print('THIS IS response on way ${response.body}');
      print(response.statusCode.toString());
      print(response.body.toString());
      _confirmPayment('Delivery status updated successfully.');
      loadInProcessButtonButton(false);
      loadOnWayButtonState(false);

      return AssignedOrderStatusOnWay.fromJson(jsonDecode(response.body));
    } else {
      print(response.statusCode.toString());
      print(response.body.toString());
      throw Exception('Failed to Assigned Order Status OnWay');
    }
  }

  //--------------------------fetchAssignedOrderStatus_Delivered----------------

  Future<AssignedOrderStatusDelivered> fetchAssignedOrderStatusDelivered(
    String token,
    String riderId,
    String orderNo,
    String status,
  ) async {
    print('THIS IS FETCH ASSIGNED ORDER STATUS DELIVERED METHOD 111');
    final http.Response response = await http.post(
      Uri.parse(BaseUrl().r_url + "rider/order/delivery-status"),
      headers: <String, String>{
        // 'Accept': 'application/json',
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json'
      },
      body: jsonEncode(<String, String>{
        'rider_id': riderId,
        'orderNo': orderNo,
        'status': status,
      }),
    );

    setState(() {
      isLoaderVisible = false;
    });
    if (response.statusCode == 200) {
      print('THIS IS FETCH ASSIGNED ORDER STATUS DELIVERED METHOD 222');

      print(response.statusCode.toString());
      print('THIS IS RESPONSE IN METHOD ${response.body}');
      print('this is order delivered or not $isOrderDelivered');
      isOrderDelivered = true;
      print('this is order delivered or not $isOrderDelivered');
      print('****** THIS IS Delivered method');
      print(response.body.toString());
      _confirmPayment('Delivery is Delivered Successfully');
      loadInProcessButtonButton(false);
      loadDeliveredButtonState(false);
      return AssignedOrderStatusDelivered.fromJson(jsonDecode(response.body));
    } else {
      print(response.statusCode.toString());
      print(response.body.toString());
      throw Exception('Failed to fetch Assigned Order Status Delivered');
    }
  }

  bool deliveredButtonState = true;
  loadDeliveredButtonState(bool value) {
    if (deliveredButtonState == value) {
      setState(() {
        deliveredButtonState = value;
      });
    } else {
      setState(() {
        deliveredButtonState = value;
      });
    }
  }

  _confirmPayment(String content) async {
    var baseDialog = BaseAlertDialog(
      title: "Order Status",
      content: "$content",
      // yesOnPressed: () {
      //   Navigator.popAndPushNamed(context, '/OrdersScreen');
      // },
      // noOnPressed: () {
      //   setState(() {
      //     paymentStatus = false;
      //   });
      //   // Navigator.popAndPushNamed(context, '/CheckoutScreen');
      // },
      // yes: "OK",
      // no: "No"
    );
    await Future.delayed(Duration(milliseconds: 50));
    // Future.delayed(Duration(seconds: 2), () {
    //   Navigator.of(context).pop(true);
    // });

    showDialog(
        context: context,
        // barrierDismissible: false,
        // color: appTheme.primaryColor
        // barrierColor: appTheme.primaryColor,
        builder: (BuildContext context) => baseDialog);
  }

  bool orderButton = true;
  loadOrderButton(bool value) {
    if (orderButton == value) {
      setState(() {
        orderButton = value;
      });
    } else {
      setState(() {
        orderButton = value;
      });
    }
  }

  bool visible = false;

  loadProgress(bool value) {
    if (visible == value) {
      setState(() {
        visible = value;
      });
    } else {
      setState(() {
        visible = value;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: WillPopScope(
          onWillPop: () {
            if (isLoaderVisible) {
              _showDialog('Processing...\nPlease wait !', false);
            } else {
              Navigator.of(context).pop();
            }
            return;
          },
          child: Stack(
            clipBehavior: Clip.none,
            children: [
              GoogleMap(
                  myLocationEnabled: false,
                  compassEnabled: false,
                  tiltGesturesEnabled: false,
                  markers: _markers,
                  myLocationButtonEnabled: false,
                  zoomControlsEnabled: true,
                  polylines: _polyLines,
                  mapType: MapType.normal,
                  initialCameraPosition: currentLocation == null
                      ? CameraPosition(
                          target: LatLng(
                              addressLat ?? 24.8923, addressLong ?? 67.2332),
                          zoom: 16)
                      : CameraPosition(
                          target: LatLng(currentLocation.latitude,
                              currentLocation.longitude),
                          zoom: 14),
                  onMapCreated: (GoogleMapController controller) {
                    // controller.setMapStyle(Utils.mapStyles);
                    _controller.complete(controller);
                  }),
              Visibility(
                visible: isLoaderVisible,
                child: Center(
                    child: CircularProgressIndicator(
                  strokeWidth: 4,
                  color: Colors.black,
                )),
              ),
              Align(
                alignment: Alignment.topCenter,
                child: GestureDetector(
                  onTap: () {
                    // Geolocator.getPositionStream(
                    //   desiredAccuracy: LocationAccuracy.lowest,
                    //   distanceFilter: 100000,
                    //   intervalDuration: Duration(seconds: 60),
                    // ).listen((position) {
                    //   // Do something here
                    //   print(
                    //       '-----------__________---------GeoLocator---------______------------__--');
                    //   print(position.latitude);
                    //   print(position.longitude);
                    //   print(
                    //       '-----------__________---------End GeoLocator---------______------------__--');
                    // });
                  },
                  child: Card(
                    elevation: 40,
                    margin: EdgeInsets.only(top: 90),
                    color: Colors.white,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: estimatedDriveTimeInMinutes != null
                          ? Text(
                              'Est Time : ${estimatedDriveTimeInMinutes.toInt()} min',
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  color: Colors.black54,
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold),
                            )
                          : Text(
                              'Est Time : 0 min',
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  color: Colors.black54,
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold),
                            ),
                    ),
                  ),
                ),
              ),
              Positioned(
                top: 12,
                left: 10,
                child: InkWell(
                  onTap: () async {
                    final GoogleMapController controller =
                        await _controller.future;
                    controller.animateCamera(
                      CameraUpdate.newCameraPosition(
                        CameraPosition(
                            target: LatLng(addressLat, addressLong), zoom: 15),
                      ),
                    );
                  },
                  child: Container(
                    height: 45,
                    width: 140,
                    decoration: new BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black54,
                            blurRadius: 6,
                            offset: const Offset(0, 5),
                            spreadRadius: 4,
                          )
                        ]),
                    child: Center(
                      child: Text(
                        'Order Location',
                        textAlign: TextAlign.center,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            color: Colors.black54,
                            fontSize: 16,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                ),
              ),
              Positioned(
                top: 16,
                right: 10,
                child: InkWell(
                  onTap: () async {
                    LocationPermission permission;
                    permission = await Geolocator.checkPermission();
                    if (permission == LocationPermission.denied) {
                      getUserCurrentLocation();
                    } else {
                      final GoogleMapController controller =
                          await _controller.future;
                      controller.animateCamera(
                        CameraUpdate.newCameraPosition(
                          CameraPosition(
                              target: LatLng(currentLocation.latitude,
                                  currentLocation.longitude),
                              zoom: 15),
                        ),
                      );
                    }
                  },
                  child: Container(
                    decoration: new BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black54,
                            blurRadius: 6,
                            offset: const Offset(0, 5),
                            spreadRadius: 4,
                          )
                        ]),
                    child: Padding(
                      padding: const EdgeInsets.all(6.0),
                      child: Icon(
                        Icons.center_focus_strong_sharp,
                        size: 24,
                        color: Colors.black54,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        bottomNavigationBar: Container(
          color: Colors.black.withOpacity(0.75),
          height: MediaQuery.of(context).size.height * 0.12,
          width: double.infinity,
          child: Visibility(
            visible: !getStorage.hasData(orderNo),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(60, 16, 60, 16),
              child: isOrderDelivered
                  ? InkWell(
                      onTap: () {
                        _showDialog('Order is already delivered !', false);
                      },
                      child: Container(
                        height: 17,
                        width: 70,
                        decoration: new BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.white70),
                        child: Center(
                          child: Text(
                            'Delivered',
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 20,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                    )
                  : Visibility(
                      visible: !isLoaderVisible,
                      child: ButtonLoginRegister(
                        buttonColor: appTheme.accentColor,
                        buttonText: 'Collect Order',
                        onPressed: () async {
                          setState(() {
                            isLoaderVisible = true;
                          });
                          final _response =
                              await fetchAssignedOrderStatusUpdated(
                                  token.toString(),
                                  riderId.toString(),
                                  technosisOrderNumber,
                                  '1');
                          if (_response.getCode == 200) {
                            print('this is collect order button hit');
                            getStorage.write(orderNo, 'collected');
                            setState(() {});
                          }
                          AlertDialog(
                            title: CircularProgressIndicator(),
                          );
                          // loadProgress(true);
                          // loadOrderButton(false);
                          // CircularProgressIndicator();
                          // ;
                        },
                      ),
                    ),
            ),
            replacement: Padding(
              padding: const EdgeInsets.fromLTRB(20, 20, 20, 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  ButtonLoginRegister(
                    buttonColor: appTheme.accentColor,
                    onPressed: () async {
                      setState(() {
                        isLoaderVisible = true;
                      });
                      final _response = await fetchAssignedOrderStatusOnWay(
                          token.toString(),
                          riderId.toString(),
                          technosisOrderNumber,
                          'on-way');

                      if (_response.getCode == 200) {
                        _showDialog('Order tracking status updated', false);
                      }
                    },
                    buttonText: 'on way',
                  ),
                  ButtonLoginRegister(
                    buttonColor: appTheme.accentColor,
                    onPressed: () async {
                      setState(() {
                        isLoaderVisible = true;
                      });
                      final _response = await fetchAssignedOrderStatusDelivered(
                        token.toString(),
                        riderId.toString(),
                        technosisOrderNumber,
                        'delivered',
                      );

                      if (_response.getCode == 200) {
                        print('THIS IS FETCH ORDER STATUS DELIVERED ON CLICK');
                        print(
                            'this is order delivered or not:  $isOrderDelivered');
                        isOrderDelivered = true;
                        print(
                            'this is order delivered or not:  $isOrderDelivered');
                        // print('status 200');
                        getStorage.remove(orderNo);
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => OrdersScreen(
                                  initialIndex: 0,
                                )));
                      }
                    },
                    buttonText: 'Delivered',
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
// import 'dart:async';
// import 'dart:convert';

// import 'package:flutter/material.dart';
// import 'package:flutter_polyline_points/flutter_polyline_points.dart';
// import 'package:geolocator/geolocator.dart';
// import 'package:get_storage/get_storage.dart';
// // import 'package:geolocator/geolocator.dart';
// import 'package:google_maps_flutter/google_maps_flutter.dart';
// import 'package:http/http.dart' as http;
// import 'package:localstorage/localstorage.dart';
// import 'package:location/location.dart';
// import 'package:rehmatshereen_rider_app/Common/BaseAlertDialog.dart';
// import 'package:rehmatshereen_rider_app/Common/Theme.dart';
// import 'package:rehmatshereen_rider_app/Components/Login_and_Register/ButtonLoginRegister.dart';
// import 'package:rehmatshereen_rider_app/model/model_AssignedOrderStatusDelivered.dart';
// import 'package:rehmatshereen_rider_app/model/model_AssignedOrderStatusInProcess.dart';
// import 'package:rehmatshereen_rider_app/model/model_AssignedOrderStatusOnWay.dart';
// import 'package:rehmatshereen_rider_app/model/model_AssignedOrderStatusUpdated.dart';
// import 'package:rehmatshereen_rider_app/screens/orders_screen.dart';
// import 'package:rehmatshereen_rider_app/utils/baseurl.dart';
// import 'package:shared_preferences/shared_preferences.dart';

// const double CAMERA_ZOOM = 12.0;
// const double CAMERA_TILT = 80;
// const double CAMERA_BEARING = 30;
// const LatLng SOURCE_LOCATION = LatLng(24.860333, 67.064573);
// const LatLng DEST_LOCATION = LatLng(24.843776, 67.055906);

// class TravelToDeliveryAddress extends StatefulWidget {
// //  final LatLng comingSourceLocation;
//   final LatLng comingDestinationLocation;
//   TravelToDeliveryAddress(
//     this.comingDestinationLocation,
//   );
//   @override
//   _TravelToDeliveryAddressState createState() =>
//       _TravelToDeliveryAddressState();
// }

// class _TravelToDeliveryAddressState extends State<TravelToDeliveryAddress> {
//   double addressLat, addressLong;
//   Set<Marker> _markers = Set<Marker>();
//   Set<Polyline> _polyLines = Set<Polyline>();
//   double distanceInMeters;
//   double estimatedDriveTimeInMinutes;
//   // CameraPosition initialCameraPosition = CameraPosition(
//   //     zoom: CAMERA_ZOOM,
//   //     // tilt: CAMERA_TILT,
//   //     // bearing: CAMERA_BEARING,
//   //     target: widget.comingSourceLocation);

//   // Future<AssignedOrderStatusUpdated> _futureAssignedOrderStatusUpdated;
//   // Future<AssignedOrderStatusInProcess> _futureFetchAssignedOrderStatusInProcess;
//   // Future<AssignedOrderStatusOnWay> _futureFetchAssignedOrderStatusOnWay;
//   // Future<AssignedOrderStatusDelivered> _futureFetchAssignedOrderStatusDelivered;

// //!-------
//   PolylinePoints polylinePoints = PolylinePoints();
//   String googleAPiKey = "AIzaSyCVEw8VKyy9ustmvw85CFrtg5BgJm1_R1U";
//   // String googleAPiKey = "AIzaSyB8LPyTTDYROup1C5Y9S7jXsAM-UKWJdyc";
//   List<LatLng> polylineCoordinates = [];
//   getPolyline(addLat, addlon) async {
//     print('----------------((((())))))_--Address');
//     print(addressLat);
//     print(addressLong);
//     print('----------------((((())))))_--Address');
//     PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
//       googleAPiKey,
//       PointLatLng(addLat, addlon),
//       // PointLatLng(SOURCE_LOCATION.latitude,SOURCE_LOCATION.longitude),
//       PointLatLng(widget.comingDestinationLocation.latitude,
//           widget.comingDestinationLocation.longitude),
//       travelMode: TravelMode.driving,
//     );
//     if (result.points.isNotEmpty) {
//       print('-------------itsresult');
//       setState(() {
//         result.points.forEach((PointLatLng point) {
//           polylineCoordinates.add(LatLng(point.latitude, point.longitude));
//         });
//       });
//     } else {
//       print('-------------its else result');
//     }
//     addPolyLine();
//   }

//   addPolyLine() {
//     print('Adding======polyline');
//     print(polylineCoordinates[0].latitude);
//     setState(() {
//       _polyLines.add(Polyline(
//         polylineId: PolylineId('id01'),
//         color: Colors.red,
//         width: 4,
//         points: polylineCoordinates,
//       ));
//     });
//   }

// //!-----

//   // SharedPreferences _prefs;
//   final getStorage = GetStorage();
//   String orderStatus;

//   String token, riderId, orderNo;
//   String technosisOrderNumber;

//   bool isOrderDelivered = false;
//   final LocalStorage storage = LocalStorage('token');

//   Completer<GoogleMapController> _controller = Completer();

//   Position currentLocation;
//   BitmapDescriptor sourceIcon;
//   BitmapDescriptor destinationIcon;
//   bool isLoaderVisible = false;

//   //* --- overrides ---

//   getDeliveryStatus() async {
//     SharedPreferences prefs = await SharedPreferences.getInstance();
//     // prefs.getString('orderDeliveredStatus');
//     orderStatus = prefs.getString('orderDeliveredStatus');
//     print('this is order status $orderStatus');
//     technosisOrderNumber = prefs.getString('technosisOrderNumber');
//     if (orderStatus == 'delivered') {
//       isOrderDelivered = true;
//     } else {
//       isOrderDelivered = false;
//     }
//     print('this is order delivered 123 $isOrderDelivered');
//     return orderStatus;
//   }

//   getLocation() async {
//     SharedPreferences prefs = await SharedPreferences.getInstance();
//     addressLat = prefs.getDouble('addressLat');
//     addressLong = prefs.getDouble('addressLong');

//     print('****************************');
//   }

//   Location location;
//   LocationData locationData;
//   @override
//   void initState() {
//     super.initState();
//     //!---
//     location = new Location();
//     location.onLocationChanged().listen((LocationData cLoc) {
//       // cLoc contains the lat and long of the
//       // current user's position in real time,
//       // so we're holding on to it
//       locationData = cLoc;
//     });
//     //!
//     getLocation();
//     setSourceAndDestinationIcons();
//     getUserCurrentLocation().then((value) {
//       addMarkers();
//       getPolyline(value.latitude, value.longitude);
//       getDistanceInMeter(widget.comingDestinationLocation,
//           LatLng(value.latitude, value.longitude));
//     });
//     if (_markers.isEmpty) {
//       setState(() {
//         isLoaderVisible = true;
//       });
//     }
//     getDeliveryStatus();
//     print('this is order delivery status ${getDeliveryStatus()}');
//     token = storage.getItem("token").toString();
//     riderId = storage.getItem("rider_id").toString();

//     orderNo = storage.getItem("technosys_order_no");
//     print('THIS IS TECHNOSIS ORDER NUMBER $orderNo');
//     print('********');
//     print('********');
//     print('********');
//     print('********');
//     print('THIS IS LAT *************** ');
//   }

//   //* --- methods ---

//   getDistanceInMeter(LatLng location1Latlong, LatLng location2Latlong) {
//     distanceInMeters = Geolocator.distanceBetween(
//         location1Latlong.latitude,
//         location1Latlong.longitude,
//         location2Latlong.latitude,
//         location2Latlong.longitude);
//     int speedIs10MetersPerMinute = 60;
//     setState(() {
//       estimatedDriveTimeInMinutes = distanceInMeters / speedIs10MetersPerMinute;
//     });

//     print('Its Drive Time---------');
//     print(estimatedDriveTimeInMinutes);
//   }

//   void setSourceAndDestinationIcons() async {
//     BitmapDescriptor.fromAssetImage(ImageConfiguration(devicePixelRatio: 2.0),
//             'Assets/Images/destination_map_marker.png')
//         .then((onValue) {
//       sourceIcon = onValue;
//     });

//     BitmapDescriptor.fromAssetImage(ImageConfiguration(devicePixelRatio: 2.0),
//             'Assets/Images/driving_pin.png')
//         .then((onValue) {
//       destinationIcon = onValue;
//     });
//   }

//   Future<Position> getUserCurrentLocation() async {
//     bool serviceEnabled;
//     LocationPermission permission;
//     serviceEnabled = await Geolocator.isLocationServiceEnabled();
//     if (!serviceEnabled) {
//       return Future.error('Location services are disabled.');
//     }

//     permission = await Geolocator.checkPermission();
//     if (permission == LocationPermission.denied) {
//       permission = await Geolocator.requestPermission();
//       if (permission == LocationPermission.denied) {
//         return Future.error('Location permissions are denied');
//       }
//     }
//     currentLocation = await Geolocator.getCurrentPosition();
//     print('this is current location $currentLocation');
//     return await Geolocator.getCurrentPosition();
//   }

//   addMarkers() async {
//     _markers.clear();
//     setState(() {
//       _markers.add(
//         Marker(
//           markerId: MarkerId('rider current location'),
//           icon: destinationIcon,
//           infoWindow: InfoWindow(title: 'Your location'),
//           position: LatLng(currentLocation.latitude, currentLocation.longitude),
//         ),
//       );
//       _markers.add(Marker(
//         markerId: MarkerId('rider destination'),
//         infoWindow: InfoWindow(title: 'This is delivery address'),
//         position: widget.comingDestinationLocation,
//         // position: LatLng(addressLat, addressLong)
//       ));
//     });
//     // if (addressLat != null && addressLong != null) {
//     setState(() {
//       isLoaderVisible = false;
//     });
//     final GoogleMapController controller = await _controller.future;
//     controller.animateCamera(
//       CameraUpdate.newCameraPosition(
//         CameraPosition(
//             target: LatLng(currentLocation.latitude, currentLocation.longitude),
//             zoom: 15),
//       ),
//     );
//     // }
//   }

//   bool inProcessButton = true;
//   loadInProcessButtonButton(bool value) {
//     if (inProcessButton == value) {
//       setState(() {
//         inProcessButton = value;
//       });
//     } else {
//       setState(() {
//         inProcessButton = value;
//       });
//     }
//   }

//   bool onWayButtonState = true;
//   loadOnWayButtonState(bool value) {
//     if (onWayButtonState == value) {
//       setState(() {
//         onWayButtonState = value;
//       });
//     } else {
//       setState(() {
//         onWayButtonState = value;
//       });
//     }
//   }

//   //--------------------------fetchAssignedOrderStatusUpdated---------------------
//   Future<AssignedOrderStatusUpdated> fetchAssignedOrderStatusUpdated(
//     String token,
//     String riderId,
//     String orderNo,
//     String status,
//   ) async {
//     final http.Response response = await http.post(
//       Uri.parse(BaseUrl().r_url + "rider/order-collect/status"),
//       headers: <String, String>{
//         // 'Accept': 'application/json',
//         'Authorization': 'Bearer ' + token,
//         'Content-Type': 'application/json'
//       },
//       body: jsonEncode(<String, String>{
//         'rider_id': riderId,
//         'orderNo': orderNo,
//         'status': status,
//       }),
//     );

//     setState(() {
//       isLoaderVisible = false;
//     });

//     if (response.statusCode == 200) {
//       print(response.statusCode.toString());
//       print(response.body.toString());
//       _confirmPayment('status updated successfully');

//       return AssignedOrderStatusUpdated.fromJson(jsonDecode(response.body));
//     } else {
//       print('this is status code ${response.statusCode.toString()}');
//       print(response.body.toString());
//       throw Exception('Failed to Assigned Order Status Updated');
//     }
//   }
// //--------------------------fetchAssignedOrderStatus_IN-process-----------------

//   Future<AssignedOrderStatusInProcess> fetchAssignedOrderStatusInProcess(
//     String token,
//     String riderId,
//     String orderNo,
//     String status,
//   ) async {
//     final http.Response response = await http.post(
//       Uri.parse(BaseUrl().r_url + "rider/order/delivery-status"),
//       headers: <String, String>{
//         // 'Accept': 'application/json',
//         'Authorization': 'Bearer ' + token,
//         'Content-Type': 'application/json'
//       },
//       body: jsonEncode(<String, String>{
//         'rider_id': riderId,
//         'orderNo': orderNo,
//         'status': status,
//       }),
//     );

//     if (response.statusCode == 200) {
//       print(response.statusCode.toString());
//       print(response.body.toString());
//       _showDialog('Delivery InProcess', false);
//       loadInProcessButtonButton(false);
//       return AssignedOrderStatusInProcess.fromJson(jsonDecode(response.body));
//     } else {
//       print(response.statusCode.toString());
//       print(response.body.toString());
//       throw Exception('Failed to Assigned Order Status In Process');
//     }
//   }

//   void _showDialog(String contentString, bool value) {
//     // flutter defined function
//     showDialog(
//       context: context,
//       builder: (BuildContext context) {
//         if (value == false) {
//           print(value);
//           Future.delayed(Duration(seconds: 1), () {
//             Navigator.of(context).pop(true);
//           });
//         } else {}

// //            Center()

// //        Future.delayed(Duration(seconds: 1), () {
// //                Navigator.of(context).pop(true);
// //              });
//         // return object of type Dialog
//         return AlertDialog(
//           elevation: 5,
//           // title: Text(
//           //   "$titlestring",
//           //   // "Sign Up Successfully",
//           //   textAlign: TextAlign.center,
//           //    style: TextStyle(
//           //         color: Colors.orange[700],
//           //       ),
//           // ),
//           content: Row(
//             mainAxisAlignment: MainAxisAlignment.center,
//             crossAxisAlignment: CrossAxisAlignment.center,
//             children: [
//               // Padding(
//               //   padding: const EdgeInsets.only(right: 3),
//               //   child: CircleAvatar(
//               //     radius: 15,
//               //     backgroundColor: Colors.orange[700],
//               //     child: SizedBox(
//               //       child: Icon(
//               //         Icons.login_rounded,
//               //         //  size: 25,
//               //         color: Colors.white,
//               //         // color: Colors.orange[700],
//               //       ),
//               //     ),
//               //   ),
//               // ),

//               Expanded(
// //                flex: 2,

//                 child: Text(
//                   "$contentString",
//                   textAlign: TextAlign.left,
//                   maxLines: 4,
//                   style: TextStyle(
//                       color: Colors.white, fontWeight: FontWeight.bold),

//                   // "Sign Up Successfully",
//                 ),
//               )
//             ],
//           ),
//         );
//       },
//     );
//   }

//   //--------------------------fetchAssignedOrderStatus_on-way-------------------

//   Future<AssignedOrderStatusOnWay> fetchAssignedOrderStatusOnWay(
//     String token,
//     String riderId,
//     String orderNo,
//     String status,
//   ) async {
//     print('THIS IS FETCH ASSIGNED ORDER STATUS ON WAY METHOD');
//     final http.Response response = await http.post(
//       Uri.parse(BaseUrl().r_url + "rider/order/delivery-status"),
//       headers: <String, String>{
//         // 'Accept': 'application/json',
//         'Authorization': 'Bearer ' + token,
//         'Content-Type': 'application/json'
//       },
//       body: jsonEncode(<String, String>{
//         'rider_id': riderId,
//         'orderNo': orderNo,
//         'status': status,
//       }),
//     );

//     setState(() {
//       isLoaderVisible = false;
//     });

//     if (response.statusCode == 200) {
//       print('THIS IS response on way ${response.body}');
//       print(response.statusCode.toString());
//       print(response.body.toString());
//       _confirmPayment('Delivery status updated successfully.');
//       loadInProcessButtonButton(false);
//       loadOnWayButtonState(false);

//       return AssignedOrderStatusOnWay.fromJson(jsonDecode(response.body));
//     } else {
//       print(response.statusCode.toString());
//       print(response.body.toString());
//       throw Exception('Failed to Assigned Order Status OnWay');
//     }
//   }

//   //--------------------------fetchAssignedOrderStatus_Delivered----------------

//   Future<AssignedOrderStatusDelivered> fetchAssignedOrderStatusDelivered(
//     String token,
//     String riderId,
//     String orderNo,
//     String status,
//   ) async {
//     print('THIS IS FETCH ASSIGNED ORDER STATUS DELIVERED METHOD 111');
//     final http.Response response = await http.post(
//       Uri.parse(BaseUrl().r_url + "rider/order/delivery-status"),
//       headers: <String, String>{
//         // 'Accept': 'application/json',
//         'Authorization': 'Bearer ' + token,
//         'Content-Type': 'application/json'
//       },
//       body: jsonEncode(<String, String>{
//         'rider_id': riderId,
//         'orderNo': orderNo,
//         'status': status,
//       }),
//     );

//     setState(() {
//       isLoaderVisible = false;
//     });
//     if (response.statusCode == 200) {
//       print('THIS IS FETCH ASSIGNED ORDER STATUS DELIVERED METHOD 222');

//       print(response.statusCode.toString());
//       print('THIS IS RESPONSE IN METHOD ${response.body}');
//       print('this is order delivered or not $isOrderDelivered');
//       isOrderDelivered = true;
//       print('this is order delivered or not $isOrderDelivered');
//       print('****** THIS IS Delivered method');
//       print(response.body.toString());
//       _confirmPayment('Delivery is Delivered Successfully');
//       loadInProcessButtonButton(false);
//       loadDeliveredButtonState(false);
//       return AssignedOrderStatusDelivered.fromJson(jsonDecode(response.body));
//     } else {
//       print(response.statusCode.toString());
//       print(response.body.toString());
//       throw Exception('Failed to fetch Assigned Order Status Delivered');
//     }
//   }

//   bool deliveredButtonState = true;
//   loadDeliveredButtonState(bool value) {
//     if (deliveredButtonState == value) {
//       setState(() {
//         deliveredButtonState = value;
//       });
//     } else {
//       setState(() {
//         deliveredButtonState = value;
//       });
//     }
//   }

//   _confirmPayment(String content) async {
//     var baseDialog = BaseAlertDialog(
//       title: "Order Status",
//       content: "$content",
//       // yesOnPressed: () {
//       //   Navigator.popAndPushNamed(context, '/OrdersScreen');
//       // },
//       // noOnPressed: () {
//       //   setState(() {
//       //     paymentStatus = false;
//       //   });
//       //   // Navigator.popAndPushNamed(context, '/CheckoutScreen');
//       // },
//       // yes: "OK",
//       // no: "No"
//     );
//     await Future.delayed(Duration(milliseconds: 50));
//     // Future.delayed(Duration(seconds: 2), () {
//     //   Navigator.of(context).pop(true);
//     // });

//     showDialog(
//         context: context,
//         // barrierDismissible: false,
//         // color: appTheme.primaryColor
//         // barrierColor: appTheme.primaryColor,
//         builder: (BuildContext context) => baseDialog);
//   }

//   bool orderButton = true;
//   loadOrderButton(bool value) {
//     if (orderButton == value) {
//       setState(() {
//         orderButton = value;
//       });
//     } else {
//       setState(() {
//         orderButton = value;
//       });
//     }
//   }

//   bool visible = false;

//   loadProgress(bool value) {
//     if (visible == value) {
//       setState(() {
//         visible = value;
//       });
//     } else {
//       setState(() {
//         visible = value;
//       });
//     }
//   }

//   @override
//   Widget build(BuildContext context) {
//     return SafeArea(
//       child: Scaffold(
//         backgroundColor: Colors.white,
//         body: WillPopScope(
//           onWillPop: () {
//             if (isLoaderVisible) {
//               _showDialog('Processing...\nPlease wait !', false);
//             } else {
//               Navigator.of(context).pop();
//             }
//             return;
//           },
//           child: Stack(
//             clipBehavior: Clip.none,
//             children: [
//               GoogleMap(
//                   myLocationEnabled: false,
//                   compassEnabled: false,
//                   tiltGesturesEnabled: false,
//                   markers: _markers,
//                   myLocationButtonEnabled: false,
//                   zoomControlsEnabled: true,
//                   polylines: _polyLines,
//                   mapType: MapType.normal,
//                   initialCameraPosition: currentLocation == null
//                       ? CameraPosition(
//                           target: LatLng(
//                               addressLat ?? 24.8923, addressLong ?? 67.2332),
//                           zoom: 14)
//                       : CameraPosition(
//                           target: LatLng(currentLocation.latitude,
//                               currentLocation.longitude),
//                           zoom: 14),
//                   onMapCreated: (GoogleMapController controller) {
//                     // controller.setMapStyle(Utils.mapStyles);
//                     _controller.complete(controller);
//                   }),
//               Visibility(
//                 visible: isLoaderVisible,
//                 child: Center(
//                     child: CircularProgressIndicator(
//                   strokeWidth: 4,
//                   color: Colors.black,
//                 )),
//               ),
//               Align(
//                 alignment: Alignment.topCenter,
//                 child: Card(
//                   elevation: 40,
//                   margin: EdgeInsets.only(top: 90),
//                   color: Colors.white,
//                   child: Padding(
//                     padding: const EdgeInsets.all(8.0),
//                     child: estimatedDriveTimeInMinutes != null
//                         ? Text(
//                             'Est Time : ${estimatedDriveTimeInMinutes.toInt()} min',
//                             maxLines: 1,
//                             overflow: TextOverflow.ellipsis,
//                             style: TextStyle(
//                                 color: Colors.black54,
//                                 fontSize: 16,
//                                 fontWeight: FontWeight.bold),
//                           )
//                         : Text(
//                             'Est Time : 0 min',
//                             maxLines: 1,
//                             overflow: TextOverflow.ellipsis,
//                             style: TextStyle(
//                                 color: Colors.black54,
//                                 fontSize: 16,
//                                 fontWeight: FontWeight.bold),
//                           ),
//                   ),
//                 ),
//               ),
//               Positioned(
//                 top: 12,
//                 left: 10,
//                 child: InkWell(
//                   onTap: () async {
//                     final GoogleMapController controller =
//                         await _controller.future;
//                     controller.animateCamera(
//                       CameraUpdate.newCameraPosition(
//                         CameraPosition(
//                             target: LatLng(addressLat, addressLong), zoom: 15),
//                       ),
//                     );
//                   },
//                   child: Container(
//                     height: 45,
//                     width: 140,
//                     decoration: new BoxDecoration(
//                         borderRadius: BorderRadius.circular(10),
//                         color: Colors.white,
//                         boxShadow: [
//                           BoxShadow(
//                             color: Colors.black54,
//                             blurRadius: 6,
//                             offset: const Offset(0, 5),
//                             spreadRadius: 4,
//                           )
//                         ]),
//                     child: Center(
//                       child: Text(
//                         'Order Location',
//                         textAlign: TextAlign.center,
//                         maxLines: 2,
//                         overflow: TextOverflow.ellipsis,
//                         style: TextStyle(
//                             color: Colors.black54,
//                             fontSize: 16,
//                             fontWeight: FontWeight.bold),
//                       ),
//                     ),
//                   ),
//                 ),
//               ),
//               Positioned(
//                 top: 16,
//                 right: 10,
//                 child: InkWell(
//                   onTap: () async {
//                     LocationPermission permission;
//                     permission = await Geolocator.checkPermission();
//                     if (permission == LocationPermission.denied) {
//                       getUserCurrentLocation();
//                     } else {
//                       final GoogleMapController controller =
//                           await _controller.future;
//                       controller.animateCamera(
//                         CameraUpdate.newCameraPosition(
//                           CameraPosition(
//                               target: LatLng(currentLocation.latitude,
//                                   currentLocation.longitude),
//                               zoom: 15),
//                         ),
//                       );
//                     }
//                   },
//                   child: Container(
//                     decoration: new BoxDecoration(
//                         borderRadius: BorderRadius.circular(10),
//                         color: Colors.white,
//                         boxShadow: [
//                           BoxShadow(
//                             color: Colors.black54,
//                             blurRadius: 6,
//                             offset: const Offset(0, 5),
//                             spreadRadius: 4,
//                           )
//                         ]),
//                     child: Padding(
//                       padding: const EdgeInsets.all(6.0),
//                       child: Icon(
//                         Icons.center_focus_strong_sharp,
//                         size: 24,
//                         color: Colors.black54,
//                       ),
//                     ),
//                   ),
//                 ),
//               ),
//             ],
//           ),
//         ),
//         bottomNavigationBar: Container(
//           color: Colors.black.withOpacity(0.75),
//           height: MediaQuery.of(context).size.height * 0.12,
//           width: double.infinity,
//           child: Visibility(
//             visible: !getStorage.hasData(orderNo),
//             child: Padding(
//               padding: const EdgeInsets.fromLTRB(60, 16, 60, 16),
//               child: isOrderDelivered
//                   ? InkWell(
//                       onTap: () {
//                         _showDialog('Order is already delivered !', false);
//                       },
//                       child: Container(
//                         height: 17,
//                         width: 70,
//                         decoration: new BoxDecoration(
//                             borderRadius: BorderRadius.circular(10),
//                             color: Colors.white70),
//                         child: Center(
//                           child: Text(
//                             'Delivered',
//                             style: TextStyle(
//                                 color: Colors.black,
//                                 fontSize: 20,
//                                 fontWeight: FontWeight.bold),
//                           ),
//                         ),
//                       ),
//                     )
//                   : Visibility(
//                       visible: !isLoaderVisible,
//                       child: ButtonLoginRegister(
//                         buttonColor: appTheme.accentColor,
//                         buttonText: 'Collect Order',
//                         onPressed: () async {
//                           setState(() {
//                             isLoaderVisible = true;
//                           });
//                           final _response =
//                               await fetchAssignedOrderStatusUpdated(
//                                   token.toString(),
//                                   riderId.toString(),
//                                   technosisOrderNumber,
//                                   '1');
//                           if (_response.getCode == 200) {
//                             print('this is collect order button hit');
//                             getStorage.write(orderNo, 'collected');
//                             setState(() {});
//                           }
//                           AlertDialog(
//                             title: CircularProgressIndicator(),
//                           );
//                           // loadProgress(true);
//                           // loadOrderButton(false);
//                           // CircularProgressIndicator();
//                           // ;
//                         },
//                       ),
//                     ),
//             ),
//             replacement: Padding(
//               padding: const EdgeInsets.fromLTRB(20, 20, 20, 20),
//               child: Row(
//                 mainAxisAlignment: MainAxisAlignment.spaceAround,
//                 children: [
//                   ButtonLoginRegister(
//                     buttonColor: appTheme.accentColor,
//                     onPressed: () async {
//                       setState(() {
//                         isLoaderVisible = true;
//                       });
//                       final _response = await fetchAssignedOrderStatusOnWay(
//                           token.toString(),
//                           riderId.toString(),
//                           technosisOrderNumber,
//                           'on-way');

//                       if (_response.getCode == 200) {
//                         _showDialog('Order tracking status updated', false);
//                       }
//                     },
//                     buttonText: 'on way',
//                   ),
//                   ButtonLoginRegister(
//                     buttonColor: appTheme.accentColor,
//                     onPressed: () async {
//                       setState(() {
//                         isLoaderVisible = true;
//                       });
//                       final _response = await fetchAssignedOrderStatusDelivered(
//                         token.toString(),
//                         riderId.toString(),
//                         technosisOrderNumber,
//                         'delivered',
//                       );

//                       if (_response.getCode == 200) {
//                         print('THIS IS FETCH ORDER STATUS DELIVERED ON CLICK');
//                         print(
//                             'this is order delivered or not:  $isOrderDelivered');
//                         isOrderDelivered = true;
//                         print(
//                             'this is order delivered or not:  $isOrderDelivered');
//                         // print('status 200');
//                         getStorage.remove(orderNo);
//                         Navigator.of(context).push(MaterialPageRoute(
//                             builder: (context) => OrdersScreen()));
//                       }
//                     },
//                     buttonText: 'Delivered',
//                   ),
//                 ],
//               ),
//             ),
//           ),
//         ),
//       ),
//     );
//   }
// }
