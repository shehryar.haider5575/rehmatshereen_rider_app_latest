import 'dart:convert';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:rehmatshereen_rider_app/Common/Theme.dart';
import 'package:rehmatshereen_rider_app/Components/Buttons/ButtonTransparent.dart';
import 'package:rehmatshereen_rider_app/Components/ImageAsAsset.dart';
import 'package:rehmatshereen_rider_app/Components/Login_and_Register/ButtonLoginRegister.dart';
import 'package:rehmatshereen_rider_app/model/model_RiderRegister.dart';
import 'package:rehmatshereen_rider_app/utils/baseurl.dart';

class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  BaseUrl baseUrl = BaseUrl();
  String responseSignUpResult = "";
  String responseRegisterResult = "";
  final nameController = TextEditingController();
  final emailController = TextEditingController();
  final contactNumberController = TextEditingController();
  final passwordController = TextEditingController();
  final confirmPasswordController = TextEditingController();
  GlobalKey<FormState> _key = new GlobalKey();
  bool visible = false;
  bool _validate = false;
  String name, email, contactNumber, password, confirmpassword;

  Future<RiderRegister> _futureRiderRegister;
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  String _deviceTokenFirebase = "Waiting for token...";

  _alreadyHaveAccount() {
    Navigator.pop(context);
  }

  loadProgress(bool value) {
    if (visible == value) {
      setState(() {
        visible = value;
      });
    } else {
      setState(() {
        visible = value;
      });
    }
  }

  void _showErrorDialog(String string) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
            "$string",
            textAlign: TextAlign.center,
          ),
        );
      },
    );

    loadProgress(false);
  }

  void _showDialog(String contentstring, bool value) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        if (value == false) {
          print(value);
          Future.delayed(Duration(seconds: 1), () {
            Navigator.of(context).pop(true);
          });
        } else {}
        return AlertDialog(
          elevation: 5,
          content: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Text(
                  "$contentstring",
                  textAlign: TextAlign.left,
                  maxLines: 4,
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                ),
              )
            ],
          ),
        );
      },
    );
  }

  @override
  void initState() {
    super.initState();
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        String string = message['notification']['body'];
        _showDialog(string.toString(), true);
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
        String string = message['notification']['body'];
        _showDialog(string.toString(), true);
//        _navigateToItemDetail(message);
//          myBackgroundMessageHandler(message);
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
        String string = message['notification']['body'];
        print("onResume-----------------------");
        String data = message['data']['data'];

        _showDialog(data.toString(), true);
//          _navigateToItemDetail(message);
//          myBackgroundMessageHandler(message);
      },
    );
    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(
            sound: true, badge: true, alert: true, provisional: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
    _firebaseMessaging.getToken().then((String token) {
      assert(token != null);
      setState(() {
        _deviceTokenFirebase = "$token";
      });
      print(_deviceTokenFirebase);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
            image: AssetImage('Assets/Images/Login_bg.png'), fit: BoxFit.cover),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: SingleChildScrollView(
          child: Form(
            key: _key,
            autovalidate: _validate,
            child: Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  ImageAsAsset(
                    image: 'Rehmat_e_Shereen_Logo.png',
                    height: 120.0,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 5, horizontal: 40),
                    child: Column(
                      children: <Widget>[
                        TextFormField(
                          controller: contactNumberController,
                          validator: validatecontactNumber,
                          keyboardType: TextInputType.number,
                          maxLength: 11,
                          // inputFormatters: <TextInputFormatter>[
                          //   FilteringTextInputFormatter.digitsOnly,
                          //   FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                          // ],
                          onSaved: (String val) {
                            contactNumber = val;
                          },
                          decoration: InputDecoration(
                            prefixIcon: Padding(
                              padding: EdgeInsets.symmetric(horizontal: 20.0),
                              child: Icon(
                                Icons.phone_iphone,
                                color: Colors.white,
                              ),
                              //   Image.asset(
                              //     'Assets/Images/profile_inactive.png',
                              //     scale: 2.5,
                              //   ),
                            ),
                            hintText: 'Contact Number',
                            hintStyle: TextStyle(color: Color(0xFFFFFFFF)),
                            border: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: appTheme.primaryColor),
                                borderRadius: BorderRadius.circular(30)),
                            enabledBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: appTheme.primaryColor),
                                borderRadius: BorderRadius.circular(30)),
                            filled: true,
                            fillColor: appTheme.primaryColor.withOpacity(0.5),
                          ),
                        ),
                        Center(
                          child: Visibility(
                              // maintainSize: true,
                              // maintainAnimation: true,
                              // maintainState: true,
                              visible: visible,
                              child: CircularProgressIndicator()),
                        ),
                        SizedBox(
                          height: 2,
                        ),
                        TextFormField(
                          controller: passwordController,
                          // validator: validatePassword,
                          //   autocorrect: false,
                          //   obscureText: true,
                          onSaved: (String val) {
                            password = val;
                          },
                          decoration: InputDecoration(
                            prefixIcon: Padding(
                              padding: EdgeInsets.symmetric(horizontal: 8.0),
                              child: Image.asset(
                                'Assets/Images/lock_icon.png',
                                scale: 2.5,
                              ),
                            ),
                            hintText: 'Password',
                            hintStyle: TextStyle(color: Color(0xFFFFFFFF)),
                            border: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: appTheme.primaryColor),
                                borderRadius: BorderRadius.circular(30)),
                            enabledBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: appTheme.primaryColor),
                                borderRadius: BorderRadius.circular(30)),
                            filled: true,
                            fillColor: appTheme.primaryColor.withOpacity(0.5),
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        TextFormField(
                          controller: confirmPasswordController,
                          // validator: validateconfirmPassword,
                          //   autocorrect: false,
                          //   obscureText: true,
                          onSaved: (String val) {
                            confirmpassword = val;
                          },
                          decoration: InputDecoration(
                            prefixIcon: Padding(
                              padding: EdgeInsets.symmetric(horizontal: 8.0),
                              child: Image.asset(
                                'Assets/Images/lock_icon.png',
                                scale: 2.5,
                              ),
                            ),
                            hintText: 'Confirm Password',
                            hintStyle: TextStyle(color: Color(0xFFFFFFFF)),
                            border: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: appTheme.primaryColor),
                                borderRadius: BorderRadius.circular(30)),
                            enabledBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: appTheme.primaryColor),
                                borderRadius: BorderRadius.circular(30)),
                            filled: true,
                            fillColor: appTheme.primaryColor.withOpacity(0.5),
                          ),
                        ),
                      ],
                    ),
                  ),

                  //CustomerSignUp
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 50),
                    child: ButtonLoginRegister(
                      onPressed: () {
                        if (passwordController.text !=
                            confirmPasswordController.text) {
                          print(passwordController.text +
                              " NotMatch " +
                              confirmPasswordController.text);
                        } else {
                          print(passwordController.text +
                              "Match" +
                              confirmPasswordController.text);
                          _sendToServer();
                        }
                      },
                      buttonText: 'Register',
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 70),
                    child: Container(
                      child: ButtonTransparent(
                        onPressed: () {
                          _alreadyHaveAccount();
                        },
                        buttonText: null,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              'Already have an account?',
                              style: TextStyle(color: Colors.white),
                            ),
                            SizedBox(
                              width: 20,
                            ),
                            Text(
                              'Login',
                              style: TextStyle(
                                color: appTheme.accentColor,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        // body: SafeArea(
        // ),
      ),
    );
  }

  //------riderRegisterAPI-------------------------------------------
  Future<RiderRegister> riderRegisterAPI(String phoneNumber, String password,
      String passwordConfirmation, String deviceToken) async {
    final http.Response response = await http.post(
      baseUrl.r_url + "rider/register",
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'contact': phoneNumber,
        'password': password,
        'password_confirmation': passwordConfirmation,
        'device_token': deviceToken,
      }),
    );
    if (response.statusCode == 200) {
      print('this is register response');
      print(response.statusCode.toString());
      print(response.body.toString());
      var parsedJson = json.decode(response.body);
      var success = RiderRegister.fromJson(parsedJson);
      Navigator.popAndPushNamed(context, '/login');
      _showDialog('Rider Created SuccessFully', false);
      return RiderRegister.fromJson(json.decode(response.body));
    } else {
      print(response.statusCode.toString());
      print(response.body.toString());
      // _showDialog('Contact Number not exist', false);
      loadProgress(false);
      throw Exception('Failed to Register the Rider');
    }
  }

  //validateNames
  String validateName(String value) {
    String patttern = r'(^[a-zA-Z ]*$)';
    RegExp regExp = new RegExp(patttern);
    if (value.length == 0) {
      return "Name is Required";
    } else if (!regExp.hasMatch(value)) {
      return "Name must be a-z and A-Z";
    }
    return null;
  }

//validateMobile
  String validatecontactNumber(String value) {
    String patttern = r'(^[0-9]*$)';
    RegExp regExp = new RegExp(patttern);
    if (value.length == 0) {
      return "Mobile is Required";
    } else if (value.length != 11) {
      return "Mobile number must 10 digits";
    } else if (!regExp.hasMatch(value)) {
      return "Mobile Number must be digits";
    }
    return null;
  }

//validateEmail
  String validateEmail(String value) {
    String pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regExp = new RegExp(pattern);
    if (value.length == 0) {
      return "Email is Required";
    } else if (!regExp.hasMatch(value)) {
      return "Invalid Email";
    } else {
      return null;
    }
  }

  bool validatePasswords(String value) {
    String pattern =
        r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$';
    RegExp regExp = new RegExp(pattern);
    return regExp.hasMatch(value);
  }

  String validatePassword(String value) {
    Pattern pattern =
        r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$';
    RegExp regex = new RegExp(pattern);
    print(value);
    if (value.isEmpty) {
      return 'Please enter password';
    } else {
      if (!regex.hasMatch(value))
        return 'Enter valid password';
      else
        return null;
    }
  }

  String validateconfirmPassword(String value) {
    Pattern pattern =
        r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$';
    RegExp regex = new RegExp(pattern);
    print("===========" + value);

    if (value.isEmpty) {
      return 'Please enter password';
    }

    // return null;
    else {
      if (!regex.hasMatch(value))
        return 'Enter valid password';
      else
        return null;
    }
  }

  _sendToServer() {
    print('this is send to server');
    if (_key.currentState.validate()) {
      _key.currentState.save();
      _futureRiderRegister = riderRegisterAPI(
          contactNumberController.text,
          // "03120252121",
          passwordController.text,
          confirmPasswordController.text,
          _deviceTokenFirebase);

      loadProgress(true);

      // print("Name $name");
      print("Mobile $contactNumber");
      // print("Email $email");
      print("Password $password");
      print("Confirm Password $confirmpassword");
      print("_deviceTokenFirebase \n" + _deviceTokenFirebase);
    } else {
      // validation error
      setState(() {
        _validate = true;
      });
    }
  }
}
