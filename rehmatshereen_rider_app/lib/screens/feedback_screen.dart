import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class FeedbackScreen extends StatefulWidget {
  @override
  _FeedbackScreenState createState() => _FeedbackScreenState();
}

class _FeedbackScreenState extends State<FeedbackScreen> {
  final _impressionList = ['very sad', 'sad', 'normal', 'happy', 'very happy'];
  final _impressionVisibilityList = [false, false, false, false, false];
  final TextEditingController _feedback = TextEditingController();

  String _userImpression;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Feedback'),
        leading: InkWell(
          onTap: () {
            Navigator.of(context).pop();
          },
          child: Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          // mainAxisAlignment: MainAxisAlignment,
          children: [
            const SizedBox(
              height: 35,
            ),
            Image.asset('Assets/Images/feedback.png'),
            const SizedBox(
              height: 30,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 18.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Column(
                    children: [
                      Visibility(
                          visible: _impressionVisibilityList[0],
                          child: Text(_impressionList[0])),
                      const SizedBox(
                        height: 2,
                      ),
                      InkWell(
                          onTap: () {
                            giveImpression(0);
                          },
                          child: Image.asset('Assets/Images/sad-emoji.png')),
                    ],
                  ),
                  Column(
                    children: [
                      Visibility(
                          visible: _impressionVisibilityList[1],
                          child: Text(_impressionList[1])),
                      InkWell(
                          onTap: () {
                            giveImpression(1);
                          },
                          child: Image.asset('Assets/Images/second-sad.png')),
                    ],
                  ),
                  Column(
                    children: [
                      Visibility(
                          visible: _impressionVisibilityList[2],
                          child: Text(_impressionList[2])),
                      InkWell(
                          onTap: () {
                            giveImpression(2);
                          },
                          child: Image.asset('Assets/Images/mid-sad.png')),
                    ],
                  ),
                  Column(
                    children: [
                      Visibility(
                          visible: _impressionVisibilityList[3],
                          child: Text(_impressionList[3])),
                      const SizedBox(
                        height: 4,
                      ),
                      InkWell(
                          onTap: () {
                            giveImpression(3);
                          },
                          child: Image.asset('Assets/Images/just-happy.png')),
                    ],
                  ),
                  Column(
                    children: [
                      Visibility(
                          visible: _impressionVisibilityList[4],
                          child: Text(_impressionList[4])),
                      const SizedBox(
                        height: 4,
                      ),
                      InkWell(
                          onTap: () {
                            giveImpression(4);
                          },
                          child: Image.asset('Assets/Images/very-happy.png')),
                    ],
                  )
                ],
              ),
            ),
            const SizedBox(
              height: 14,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black26,
                        blurRadius: 8,
                        spreadRadius: 8,
                        offset: Offset(6, 6),
                      )
                    ]),
                child: TextFormField(
                  maxLines: 8,
                  style: TextStyle(color: Colors.white60),
                  decoration: InputDecoration(
                      hintText: 'Type your feedback here...',
                      filled: true,
                      fillColor: Colors.black45,
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: BorderSide(color: Colors.black),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10),
                        borderSide: BorderSide(color: Colors.black),
                      )),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            ElevatedButton(onPressed: () {}, child: Text('FeedBack'))
          ],
        ),
      ),
    );
  }

  giveImpression(index) {
    for (int i = 0; i < _impressionVisibilityList.length; i++) {
      if (i == index) {
        setState(() {
          _impressionVisibilityList[i] = !_impressionVisibilityList[i];
          _userImpression = _impressionList[i];
        });
      } else {
        setState(() {
          _impressionVisibilityList[i] = false;
        });
      }
    }
    print('this is impression $_userImpression');
  }

  Future postFeedback() async {
    var url = '165.227.69.207/rehmat-e-sheree/public/api/feedback';
    var response = await http.post(
      Uri.parse(url),
      headers: {'accept': 'application/json'},
    );
  }
}
