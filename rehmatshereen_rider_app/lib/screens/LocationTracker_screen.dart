// import 'dart:async';
//
// import 'package:flutter/material.dart';
// import 'package:flutter_polyline_points/flutter_polyline_points.dart';
// import 'package:geocoding/geocoding.dart';
// import 'package:google_maps_flutter/google_maps_flutter.dart';
// import 'package:localstorage/localstorage.dart';
// import 'package:location/location.dart';
// import 'package:rehmatshereen_rider_app/Components/pin_pill_info.dart';
// import 'package:url_launcher/url_launcher.dart';
//
// const double CAMERA_ZOOM = 16;
// const double CAMERA_TILT = 80;
// const double CAMERA_BEARING = 30;
// const LatLng SOURCE_LOCATION = LatLng(42.747932, -71.167889);
// const LatLng DEST_LOCATION = LatLng(37.335685, -122.0605916);
//
// class LocationTracker extends StatefulWidget {
//   @override
//   _LocationTrackerState createState() => _LocationTrackerState();
// }
//
// class _LocationTrackerState extends State<LocationTracker> {
//   Completer<GoogleMapController> _controller = Completer();
//
//   static const LatLng _center = const LatLng(24.8603136, 67.0645627);
//   LatLng _lastMapPosition = _center;
//   List<Marker> allMarkers;
//   final LocalStorage storage = new LocalStorage('token');
//   String _address = "";
//   MapType _currentMapType = MapType.normal;
//
//   //---------------------
//   Set<Marker> _markers = Set<Marker>();
// // for my drawn routes on the map
//   Set<Polyline> _polylines = Set<Polyline>();
//   List<LatLng> polylineCoordinates = [];
//   PolylinePoints polylinePoints;
//   String googleAPIKey = 'AIzaSyB8LPyTTDYROup1C5Y9S7jXsAM-UKWJdyc';
// // for my custom marker pins
//   BitmapDescriptor sourceIcon;
//   BitmapDescriptor destinationIcon;
// // the user's initial location and current location
// // as it moves
//   LocationData currentLocation;
// // a reference to the destination location
//   LocationData destinationLocation;
// // wrapper around the location API
// //   Location location;
//   double pinPillPosition = -100;
//   PinInformation currentlySelectedPin = PinInformation(
//       pinPath: '',
//       avatarPath: '',
//       location: LatLng(0, 0),
//       locationName: '',
//       labelColor: Colors.grey);
//   PinInformation sourcePinInfo;
//   PinInformation destinationPinInfo;
//   //---------------------
//   void _onCameraMove(CameraPosition position) {
//     // position.target.latitude;
//
//     _lastMapPosition = position.target;
//   }
//
//   void _onMapCreated(GoogleMapController controller) {
//     _controller.complete(controller);
//     setMapPins();
//     setPolylines();
//     // my map has completed being created;
//     // i'm ready to show the pins on the map
//     showPinsOnMap();
//   }
//
//   // Location location = Location();
//
//   // Map<String, double> currentLocation;
//   @override
//   void initState() {
//     super.initState();
//     // location = new Location();
//
//     allMarkers = List<Marker>();
//
//     allMarkers.add(Marker(
//       markerId: MarkerId("MarkerId"),
//       draggable: false,
//       infoWindow: InfoWindow(
//         title: "saaddadada",
//         onTap: () {
//           _getPlace();
//         },
//       ),
//       onTap: () {},
//       position: LatLng(_lastMapPosition.latitude, _lastMapPosition.longitude),
//     ));
//     polylinePoints = PolylinePoints();
//     // create an instance of Location
//     // location = new Location();
//
//     // subscribe to changes in the user's location
//     // by "listening" to the location's onLocationChanged event
//     // location.onLocationChanged().listen((LocationData cLoc) {
//     //   // cLoc contains the lat and long of the
//     //   // current user's position in real time,
//     //   // so we're holding on to it
//     //   currentLocation = cLoc;
//     //   updatePinOnMap();
//     // });
//     // set custom marker pins
//     setSourceAndDestinationIcons();
//     // set the initial location
//     // setInitialLocation();
//     _getPlace();
//   }
//
//   void setSourceAndDestinationIcons() async {
//     BitmapDescriptor.fromAssetImage(
//             ImageConfiguration(devicePixelRatio: 2.0), 'assets/driving_pin.png')
//         .then((onValue) {
//       sourceIcon = onValue;
//     });
//
//     BitmapDescriptor.fromAssetImage(ImageConfiguration(devicePixelRatio: 2.0),
//             'assets/destination_map_marker.png')
//         .then((onValue) {
//       destinationIcon = onValue;
//     });
//   }
//
//   // void setInitialLocation() async {
//   //   // set the initial location by pulling the user's
//   //   // current location from the location's getLocation()
//   //   currentLocation = await location.getLocation();
//   //
//   //   // hard-coded destination for this example
//   //   destinationLocation = LocationData.fromMap({
//   //     "latitude": DEST_LOCATION.latitude,
//   //     "longitude": DEST_LOCATION.longitude
//   //   });
//   // }
//
//   void _getPlace() async {
//     // List<Placemark> newPlace = await _geolocator.placemarkFromCoordinates(
//     //     _lastMapPosition.latitude, _lastMapPosition.longitude);
//
//     List<Placemark> newPlace = await placemarkFromCoordinates(
//         _lastMapPosition.latitude, _lastMapPosition.longitude);
//     // this is all you need
//     Placemark placeMark = newPlace[0];
//     String name = placeMark.name;
//     String subLocality = placeMark.subLocality;
//     String locality = placeMark.locality;
//     String administrativeArea = placeMark.administrativeArea;
//     String postalCode = placeMark.postalCode;
//     String country = placeMark.country;
//     String street = placeMark.street;
//     String address =
//         "${name}, ${subLocality}, ${locality}, ${administrativeArea} ${postalCode}, ${country}";
//     // Block 6    ,  PECHS             , Karachi    , Sindh      , Pakistan
//
//     storage.setItem("DeliveryCityArea", name + subLocality.toString());
//     storage.setItem("DeliveryCity", locality.toString());
//     storage.setItem("DeliveryCountry", country.toString());
//     storage.setItem("address", address.toString());
//
//     print("DeliveryCityArea: " + name + " " + subLocality.toString());
//     print("DeliveryCity: " + locality.toString());
//     print("DeliveryCountry: " + country);
//     print(address);
//
//     setState(() {
//       _address = address; // update _address
//     });
//   }
//
//   _launchCaller() async {
//     const url = "tel:1234567";
//     if (await canLaunch(url)) {
//       await launch(url);
//     } else {
//       throw 'Could not launch $url';
//     }
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       child: Scaffold(
//         body: SafeArea(
//           child: ListView(
//             children: <Widget>[
//               Stack(
//                 children: <Widget>[
//                   Container(
//                     height: 350.0,
//                     // decoration: BoxDecoration(
//                     //   image: DecorationImage(
//                     //     image: AssetImage('Assets/Images/maps.jpg'),
//                     //     fit: BoxFit.cover,
//                     //   ),
//                     // ),
//                     child: GoogleMap(
//                       myLocationEnabled: true,
//                       zoomControlsEnabled: false,
//                       zoomGesturesEnabled: true,
//                       onMapCreated: _onMapCreated,
//                       myLocationButtonEnabled: true,
//                       scrollGesturesEnabled: true,
//                       tiltGesturesEnabled: true,
//                       rotateGesturesEnabled: true,
//                       initialCameraPosition: CameraPosition(
//                         target: _center,
//                         zoom: 15.0,
//                       ),
//
//                       mapType: _currentMapType,
//                       //   markers: _markers,
//                       markers: Set.from(allMarkers),
//
//                       onCameraMove: _onCameraMove,
//                     ),
//                   ),
//                   Container(
//                     // color: Colors.amber,
//                     height: 420.0,
//                     alignment: Alignment.bottomCenter,
//                     // child: Stack(
//                     //   alignment: Alignment.centerRight,
//                     //   children: <Widget>[
//                     //     Card(
//                     //       elevation: 0.0,
//                     //       margin: EdgeInsets.symmetric(horizontal: 15.0),
//                     //       shape: RoundedRectangleBorder(
//                     //         borderRadius: BorderRadius.all(
//                     //           Radius.circular(10.0),
//                     //         ),
//                     //       ),
//                     //       child: Container(
//                     //         // color: Colors.blue,
//                     //         // width: 220.0,
//                     //         width: MediaQuery.of(context).size.width * .7,
//                     //         decoration: BoxDecoration(
//                     //           color: appTheme.primaryColor,
//                     //           borderRadius: BorderRadius.all(
//                     //             Radius.circular(10.0),
//                     //           ),
//                     //           boxShadow: [
//                     //             BoxShadow(
//                     //               color: Colors.black12,
//                     //               blurRadius: 10.0,
//                     //               spreadRadius: 0.0,
//                     //               offset: Offset(0.0, 5.0),
//                     //             ),
//                     //           ],
//                     //         ),
//                     //         padding: const EdgeInsets.symmetric(
//                     //             vertical: 15.0, horizontal: 20.0),
//                     //         child: Column(
//                     //           mainAxisSize: MainAxisSize.min,
//                     //           crossAxisAlignment: CrossAxisAlignment.start,
//                     //           children: <Widget>[
//                     //             Padding(
//                     //               padding:
//                     //                   const EdgeInsets.symmetric(vertical: 5.0),
//                     //               child: Text(
//                     //                 'Tracking Details',
//                     //                 style: TextStyle(
//                     //                     fontSize: 16.0,
//                     //                     fontWeight: FontWeight.bold),
//                     //               ),
//                     //             ),
//                     //             Text('1kg Kale Gulab Jamun'),
//                     //             SizedBox(
//                     //               height: 15.0,
//                     //             ),
//                     //             Padding(
//                     //               padding:
//                     //                   const EdgeInsets.symmetric(vertical: 5.0),
//                     //               child: Text(
//                     //                 'Address',
//                     //                 style: TextStyle(
//                     //                     fontSize: 16.0,
//                     //                     fontWeight: FontWeight.bold),
//                     //               ),
//                     //             ),
//                     //             Text(
//                     //               _address,
//                     //               // 'North Karachi 11-b.'
//                     //             ),
//                     //           ],
//                     //         ),
//                     //       ),
//                     //     ),
//                     //     Container(
//                     //       child: InkWell(
//                     //         child: CircleAvatar(
//                     //             backgroundColor: kPrimaryColor,
//                     //             child: Icon(
//                     //               Icons.call,
//                     //               color: Colors.white,
//                     //             )),
//                     //         onTap: () {
//                     //           _launchCaller();
//                     //         },
//                     //       ),
//                     //     ),
//                     //   ],
//                     // ),
//                   ),
//                   Container(
//                     // color: Colors.amber,
//                     // height: 620.0,
//                     height: MediaQuery.of(context).size.height * .77,
//                     alignment: Alignment.bottomCenter,
//                     child: Stack(
//                       alignment: Alignment.centerRight,
//                       children: <Widget>[
//                         Card(
//                           elevation: 0.0,
//                           margin: EdgeInsets.symmetric(horizontal: 15.0),
//                           shape: RoundedRectangleBorder(
//                             borderRadius: BorderRadius.all(
//                               Radius.circular(10.0),
//                             ),
//                           ),
//                           // child: Container(
//                           //   height: MediaQuery.of(context).size.height / 13,
//                           //   // color: Colors.yellow,
//                           //   width: MediaQuery.of(context).size.width * .5,
//                           //   decoration: BoxDecoration(
//                           //     // color: appTheme.primaryColor
//                           //     color: Colors.yellow,
//                           //     borderRadius: BorderRadius.all(
//                           //       Radius.circular(10.0),
//                           //     ),
//                           //     boxShadow: [
//                           //       BoxShadow(
//                           //         color: Colors.black12,
//                           //         // color: Colors.yellow[50],
//                           //         blurRadius: 10.0,
//                           //         spreadRadius: 0.0,
//                           //         offset: Offset(0.0, 5.0),
//                           //       ),
//                           //     ],
//                           //   ),
//                           //   padding: const EdgeInsets.symmetric(
//                           //       vertical: 0.0, horizontal: 0.0),
//                           //   child: ButtonLoginRegister(
//                           //     onPressed: () {
//                           //       Navigator.pushNamed(context, '/CheckoutScreen');
//                           //       // Navigator.push(
//                           //       //     context,
//                           //       //     MaterialPageRoute(
//                           //       //       builder: (context) => (
//                           //       //         fromCart: 2,
//                           //       //       ),
//                           //       //     ));
//                           //     },
//                           //     buttonText: 'Check Out Details',
//                           //   ),
//                           // ),
//                         ),
//                       ],
//                     ),
//                   ),
//                   // Positioned(
//                   //   bottom: Alignment.bottomCenter.x,
//                   //   height: 100,
//                   //   // height: 200,
//                   //   // bottom: 600,
//                   //   child: Container(
//                   //     alignment: Alignment.bottomCenter,
//                   //     color: Colors.red,
//                   //     // height: MediaQuery.of(context).size.height,
//                   //   ),
//                   // ),
//                 ],
//               ),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
//
//   void showPinsOnMap() {
//     // get a LatLng for the source location
//     // from the LocationData currentLocation object
//     var pinPosition =
//         LatLng(currentLocation.latitude, currentLocation.longitude);
//     // get a LatLng out of the LocationData object
//     var destPosition =
//         LatLng(destinationLocation.latitude, destinationLocation.longitude);
//
//     sourcePinInfo = PinInformation(
//         locationName: "Start Location",
//         location: SOURCE_LOCATION,
//         pinPath: "assets/driving_pin.png",
//         avatarPath: "assets/friend1.jpg",
//         labelColor: Colors.blueAccent);
//
//     destinationPinInfo = PinInformation(
//         locationName: "End Location",
//         location: DEST_LOCATION,
//         pinPath: "assets/destination_map_marker.png",
//         avatarPath: "assets/friend2.jpg",
//         labelColor: Colors.purple);
//
//     // add the initial source location pin
//     _markers.add(Marker(
//         markerId: MarkerId('sourcePin'),
//         position: pinPosition,
//         onTap: () {
//           setState(() {
//             currentlySelectedPin = sourcePinInfo;
//             pinPillPosition = 0;
//           });
//         },
//         icon: sourceIcon));
//     // destination pin
//     _markers.add(Marker(
//         markerId: MarkerId('destPin'),
//         position: destPosition,
//         onTap: () {
//           setState(() {
//             currentlySelectedPin = destinationPinInfo;
//             pinPillPosition = 0;
//           });
//         },
//         icon: destinationIcon));
//     // set the route lines on the map from source to destination
//     // for more info follow this tutorial
//     setPolylines();
//   }
//
//   void setPolylines() async {
//     List<PointLatLng> result = await polylinePoints.getRouteBetweenCoordinates(
//         googleAPIKey,
//         currentLocation.latitude,
//         currentLocation.longitude,
//         destinationLocation.latitude,
//         destinationLocation.longitude);
//
//     if (result.isNotEmpty) {
//       result.forEach((PointLatLng point) {
//         polylineCoordinates.add(LatLng(point.latitude, point.longitude));
//       });
//
//       setState(() {
//         _polylines.add(Polyline(
//             width: 2, // set the width of the polylines
//             polylineId: PolylineId("poly"),
//             color: Color.fromARGB(255, 40, 122, 198),
//             points: polylineCoordinates));
//       });
//     }
//   }
//
//   void setMapPins() {
//     setState(() {
//       // source pin
//       _markers.add(Marker(
//           markerId: MarkerId("sourcePin"),
//           position: SOURCE_LOCATION,
//           icon: sourceIcon));
//       // destination pin
//       _markers.add(Marker(
//           markerId: MarkerId("sourcePin"),
//           position: DEST_LOCATION,
//           icon: destinationIcon));
//     });
//   }
//
//   Polylines() async {
//     List<PointLatLng> result = await polylinePoints?.getRouteBetweenCoordinates(
//         googleAPIKey,
//         SOURCE_LOCATION.latitude,
//         SOURCE_LOCATION.longitude,
//         DEST_LOCATION.latitude,
//         DEST_LOCATION.longitude);
//     if (result.isNotEmpty) {
//       // loop through all PointLatLng points and convert them
//       // to a list of LatLng, required by the Polyline
//       result.forEach((PointLatLng point) {
//         polylineCoordinates.add(LatLng(point.latitude, point.longitude));
//       });
//     }
//     setState(() {
//       // create a Polyline instance
//       // with an id, an RGB color and the list of LatLng pairs
//       Polyline polyline = Polyline(
//           polylineId: PolylineId("poly"),
//           color: Color.fromARGB(255, 40, 122, 198),
//           points: polylineCoordinates);
//
//       // add the constructed polyline as a set of points
//       // to the polyline set, which will eventually
//       // end up showing up on the map
//       _polylines.add(polyline);
//     });
//   }
//
//   void updatePinOnMap() async {
//     // create a new CameraPosition instance
//     // every time the location changes, so the camera
//     // follows the pin as it moves with an animation
//     CameraPosition cPosition = CameraPosition(
//       zoom: CAMERA_ZOOM,
//       tilt: CAMERA_TILT,
//       bearing: CAMERA_BEARING,
//       target: LatLng(currentLocation.latitude, currentLocation.longitude),
//     );
//     final GoogleMapController controller = await _controller.future;
//     controller.animateCamera(CameraUpdate.newCameraPosition(cPosition));
//     // do this inside the setState() so Flutter gets notified
//     // that a widget update is due
//     setState(() {
//       // updated position
//       var pinPosition =
//           LatLng(currentLocation.latitude, currentLocation.longitude);
//
//       sourcePinInfo.location = pinPosition;
//
//       // the trick is to remove the marker (by id)
//       // and add it again at the updated location
//       _markers.removeWhere((m) => m.markerId.value == 'sourcePin');
//       _markers.add(Marker(
//           markerId: MarkerId('sourcePin'),
//           onTap: () {
//             setState(() {
//               currentlySelectedPin = sourcePinInfo;
//               pinPillPosition = 0;
//             });
//           },
//           position: pinPosition, // updated position
//           icon: sourceIcon));
//     });
//   }
// }
