import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:localstorage/localstorage.dart';
import 'package:rehmatshereen_rider_app/Common/BaseAlertDialog.dart';
import 'package:rehmatshereen_rider_app/Common/Theme.dart';
import 'package:rehmatshereen_rider_app/Components/AppBarSimple.dart';
import 'package:rehmatshereen_rider_app/model/model_RiderAssignedOrders.dart';
import 'package:rehmatshereen_rider_app/screens/Login_screen.dart';
import 'package:rehmatshereen_rider_app/screens/feedBack_Screen.dart';
import 'package:rehmatshereen_rider_app/screens/order_history.dart';
import 'package:rehmatshereen_rider_app/screens/rider_details.dart';
import 'package:rehmatshereen_rider_app/screens/travel_to_delivery_address_screen.dart';
// import 'package:rehmatshereen_rider_app/screens/travelToDeliveryAddress.dart';
import 'package:rehmatshereen_rider_app/utils/baseurl.dart';
import 'package:shared_preferences/shared_preferences.dart';

class OrdersScreen extends StatefulWidget {
  final int initialIndex;
  OrdersScreen({@required this.initialIndex});

  @override
  _OrdersScreenState createState() => _OrdersScreenState();
}

class _OrdersScreenState extends State<OrdersScreen>
    with SingleTickerProviderStateMixin {
  BaseUrl baseUrl = BaseUrl();
  String token;
  String stringValue;
  String id, orderNo, riderId;
  bool hasConnection = false;
  String orderStatus;
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  final LocalStorage storage = LocalStorage('token');
  Future<RiderAssignedOrders> futureRiderAssignedOrders;
  StreamController connectionChangeController =
      new StreamController.broadcast();
  TabController _tabController;
  List<dynamic> collectedOrdersList = [];
  List<dynamic> deliveredOrdersList = [];

  @override
  void initState() {
    super.initState();
    _tabController = TabController(
        length: 3, vsync: this, initialIndex: widget.initialIndex ?? 0);
    // token = getStringValuesSF('token').toString();
    print('THIS IS INIT FUNCTION');
    token = storage.getItem("token").toString();
    riderId = storage.getItem("rider_id");

    print('----');
    print('this is rider id $riderId');
    print('-------------------0000000-----------');
    print(token);
    print('THIS IS RIDER ID $riderId');
    print('THIS IS TOKEN $token');
    futureRiderAssignedOrders = fetchRiderAssignedOrdersAPI(token, riderId);

    print(
        'THIS IS FUTURE RIDER ASSIGNED RESPONSE IN INIT $futureRiderAssignedOrders');
    // checkConnection();
  }

  filterOrderList(Data data) {
    setState(() {
      if (data.deliveryStatus == 'delivered') {
        deliveredOrdersList.add(data);
        orderStatus = 'Delivered';
      } else {
        collectedOrdersList.add(data);
        orderStatus = 'Collected';
      }
    });
  }

  getStringValuesSF(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    stringValue = prefs.getString(key);
    print("getStringValuesSF: $stringValue");
    print('---------------------');
    print("username " + prefs.getString('token').toString());
    print('---------------------');
    if (prefs.getString('token') == null) {
      print('IF');
      print(token);

      // Timer(Duration(seconds: 2), () => MyNavigator.goToLoginScreen(context));
    } else {
      print('ELSE');
      print(token);
      // userDetailsDataApi(prefs.getString('token').toString());

//      Timer(Duration(seconds: 2), () => MyNavigator.goToBottomNavBar(context));
    }

    return stringValue.toString();
  }

  void startServiceInPlatform() async {
    if (Platform.isAndroid) {
      var methodChannel = MethodChannel("com.example.rehmateshereen");
      String data = await methodChannel.invokeMethod("startService");
      debugPrint(data);
    }
  }

  //The test to actually see if there is a connection
  Future<bool> checkConnection() async {
    bool previousConnection = hasConnection;

    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        hasConnection = true;
      } else {
        hasConnection = false;
      }
    } on SocketException catch (_) {
      hasConnection = false;
    }

    //The connection status changed send out an update to all listeners
    if (previousConnection != hasConnection) {
      connectionChangeController.add(hasConnection);
    }

    return hasConnection;
  }

  final GlobalKey<ScaffoldState> _key = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        key: _key,
        //! -------***Drawer
        drawer: Drawer(
          child: Column(
            children: [
              Container(
                height: 200,
                width: MediaQuery.of(context).size.width,
                color: Colors.white38,
              ),
              //! ----UserProfile
              InkWell(
                onTap: () async {
                  SharedPreferences prefs =
                      await SharedPreferences.getInstance();
                  prefs.clear();
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => RiderDetailsScreen()));
                },
                child: ListTile(
                  leading: Icon(
                    Icons.person_outline_rounded,
                    color: Colors.white54,
                  ),
                  title: Text(
                    'Profile',
                    style: TextStyle(color: Colors.white54),
                  ),
                  trailing: Icon(
                    Icons.arrow_forward,
                    color: Colors.white54,
                  ),
                ),
              ),
              //! ----Order History
              InkWell(
                onTap: () {
                  Navigator.of(context).push(
                      MaterialPageRoute(builder: (context) => OrderHistory()));
                },
                child: ListTile(
                  leading: Icon(
                    Icons.history,
                    color: Colors.white54,
                  ),
                  title: Text(
                    'Order History',
                    style: TextStyle(color: Colors.white54),
                  ),
                  trailing: Icon(
                    Icons.arrow_forward,
                    color: Colors.white54,
                  ),
                ),
              ),
              //! ----Collected
              InkWell(
                onTap: () {
                  Navigator.of(context).pushReplacement(MaterialPageRoute(
                      builder: (context) => OrdersScreen(
                            initialIndex: 1,
                          )));
                },
                child: ListTile(
                  leading: Icon(
                    Icons.feedback_outlined,
                    color: Colors.white54,
                  ),
                  title: Text(
                    'Collected',
                    style: TextStyle(color: Colors.white54),
                  ),
                  trailing: Icon(
                    Icons.arrow_forward,
                    color: Colors.white54,
                  ),
                ),
              ),
              //! ----Delivered
              InkWell(
                onTap: () {
                  Navigator.of(context).pushReplacement(MaterialPageRoute(
                      builder: (context) => OrdersScreen(
                            initialIndex: 2,
                          )));
                },
                child: ListTile(
                  leading: Icon(
                    Icons.delivery_dining_outlined,
                    color: Colors.white54,
                  ),
                  title: Text(
                    'Delivered',
                    style: TextStyle(color: Colors.white54),
                  ),
                  trailing: Icon(
                    Icons.arrow_forward,
                    color: Colors.white54,
                  ),
                ),
              ),
              //! ----FeedBack
              InkWell(
                onTap: () {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => FeedbackScreen()));
                },
                child: ListTile(
                  leading: Icon(
                    Icons.login_outlined,
                    color: Colors.white54,
                  ),
                  title: Text(
                    'Feedback',
                    style: TextStyle(color: Colors.white54),
                  ),
                  trailing: Icon(
                    Icons.arrow_forward,
                    color: Colors.white54,
                  ),
                ),
              ),
              //! ----Logout
              InkWell(
                onTap: () {
                  Navigator.of(context).pushReplacement(
                      MaterialPageRoute(builder: (context) => Login()));
                },
                child: ListTile(
                  leading: Icon(
                    Icons.login_outlined,
                    color: Colors.white54,
                  ),
                  title: Text(
                    'Logout',
                    style: TextStyle(color: Colors.white54),
                  ),
                  trailing: Icon(
                    Icons.arrow_forward,
                    color: Colors.white54,
                  ),
                ),
              ),
            ],
          ),
        ),
        appBar: appBarSimple(
            onRefresh: () {
              // fetchRiderAssignedOrdersAPI(token, riderId);
              Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (context) => OrdersScreen(
                        initialIndex: 0,
                      )));
            },
            title: 'Orders',
            onTap: () {
              _key.currentState.openDrawer();
              // Navigator.of(context).pushReplacement(
              //     MaterialPageRoute(builder: (context) => Login()));
            }),
        body: SafeArea(
          child: Column(
            children: [
              Container(
                color: appTheme.primaryColor,
                height: 50,
                child: TabBar(
                  indicatorColor: appTheme.canvasColor,
                  controller: _tabController,
                  tabs: [
                    Text(
                      'Total',
                      style: TextStyle(fontSize: 17),
                    ),
                    Text(
                      'Collected',
                      style: TextStyle(fontSize: 17),
                    ),
                    Text(
                      'Delivered',
                      style: TextStyle(fontSize: 17),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: TabBarView(controller: _tabController, children: [
                  //!--------------Total
                  FutureBuilder<RiderAssignedOrders>(
                    future: futureRiderAssignedOrders,
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        if (snapshot.data.data.isEmpty) {
                          return Container(
                            color: Colors.transparent,
                            child: Center(
                              child: Text(
                                'No Orders',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18.0),
                              ),
                            ),
                          );
                        }
                        return ListView.builder(
                          itemCount: snapshot.data.data.length,
                          itemBuilder: (BuildContext context, int index) {
                            return InkWell(
                              onTap: () async {
                                storeLocation(
                                    snapshot.data.data[index].order
                                            .addressLat ??
                                        24.9453,
                                    snapshot.data.data[index].order
                                            .addressLong ??
                                        64.2342);
                                SharedPreferences preferences =
                                    await SharedPreferences.getInstance();
                                preferences.setString('technosisOrderNumber',
                                    snapshot.data.data[index].technosysOrderNo);
                                storeOrderStatus(
                                    snapshot.data.data[index].deliveryStatus);
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) =>
                                        TravelToDeliveryAddress(LatLng(
                                            snapshot.data.data[index].order
                                                .addressLat,
                                            snapshot.data.data[index].order
                                                .addressLong))));
                              },
                              child: Container(
                                height: 100,
                                margin: EdgeInsets.symmetric(
                                    vertical: 3, horizontal: 5.0),
                                decoration: BoxDecoration(
                                    color: appTheme.primaryColor,
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(10.0),
                                    ),
                                    boxShadow: [
                                      BoxShadow(
                                        color: appTheme.primaryColor,
                                        blurRadius: 10.0,
                                        spreadRadius: 0.0,
                                        offset: Offset(0.0, 5.0),
                                      ),
                                    ]),
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 8.0, vertical: 8),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Container(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                .4,
                                            child: Text(
                                              // snapshot.data.data[index].order
                                              //         .technosysOrderNo ??
                                              snapshot.data.data[index].order ==
                                                      null
                                                  ? ''
                                                  : snapshot.data.data[index]
                                                      .technosysOrderNo,
                                              maxLines: 3,
                                              style: kCollectionItemName,
                                            ),
                                          ),
                                          Text(
                                            snapshot.data.data[index].order ==
                                                    null
                                                ? ''
                                                : snapshot.data.data[index]
                                                    .order.phone,
                                            // 'Phone ${snapshot.data.data[index].order.phone}',
                                            style: kCollectionItemPrice,
                                          ),
                                        ],
                                      ),
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Container(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                .4,
                                            child: Text(
                                              // snapshot.data.data[index].order
                                              //         .technosysOrderNo ??
                                              snapshot.data.data[index].order ==
                                                      null
                                                  ? ''
                                                  : snapshot.data.data[index]
                                                      .createdAt,
                                              maxLines: 3,
                                              style: kCollectionItemName,
                                            ),
                                          ),
                                          SizedBox(
                                            height: 20,
                                          ),
                                          Text(
                                            snapshot.data.data[index].order ==
                                                    null
                                                ? ''
                                                : 'Rs. ' +
                                                    snapshot.data.data[index]
                                                        .order.totalAmount,
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                color: Colors.grey),
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            );
                          },
                        );
                      } else if (snapshot.hasError) {
                        return Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 15.0),
                          child: Center(
                            child: Text(
                              "${snapshot.error}",
                              textAlign: TextAlign.center,
                            ),
                          ),
                        );
                      }
                      // By default, show a loading spinner.
                      return Center(
                          child: CircularProgressIndicator(
                        color: Colors.white,
                      ));
                    },
                  ),
                  //!--------------Collected
                  collectedOrdersList.isEmpty
                      ? Center(
                          child: Text(
                            'No Collected Orders',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 18),
                          ),
                        )
                      : ListView.builder(
                          itemCount: collectedOrdersList.length,
                          itemBuilder: (context, index) {
                            return InkWell(
                              onTap: () async {
                                storeLocation(
                                    collectedOrdersList[index]['order']
                                            ['address_lat'] ??
                                        24.9453,
                                    collectedOrdersList[index]['order']
                                            ['address_long'] ??
                                        64.2342);
                                SharedPreferences preferences =
                                    await SharedPreferences.getInstance();
                                preferences.setString(
                                    'technosisOrderNumber',
                                    collectedOrdersList[index]
                                        ['technosys_order_no']);
                                storeOrderStatus(collectedOrdersList[index]
                                    ['delivery_status']);
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) =>
                                        TravelToDeliveryAddress(LatLng(
                                            collectedOrdersList[index]['order']
                                                ['address_lat'],
                                            collectedOrdersList[index]['order']
                                                ['address_long']))));
                              },
                              child: Container(
                                height: 100,
                                margin: EdgeInsets.symmetric(
                                    vertical: 3, horizontal: 5.0),
                                decoration: BoxDecoration(
                                    color: appTheme.primaryColor,
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(10.0),
                                    ),
                                    boxShadow: [
                                      BoxShadow(
                                        color: appTheme.primaryColor,
                                        blurRadius: 10.0,
                                        spreadRadius: 0.0,
                                        offset: Offset(0.0, 5.0),
                                      ),
                                    ]),
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 8.0, vertical: 8),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Container(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                .4,
                                            child: Text(
                                              collectedOrdersList[index]
                                                  ['technosys_order_no'],
                                              maxLines: 3,
                                              style: kCollectionItemName,
                                            ),
                                          ),
                                          Text(
                                            collectedOrdersList[index]['order']
                                                ['phone'],
                                            style: kCollectionItemPrice,
                                          ),
                                        ],
                                      ),
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Container(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                .4,
                                            child: Text(
                                              collectedOrdersList[index]
                                                      ['created_at']
                                                  .toString(),
                                              maxLines: 3,
                                              style: kCollectionItemName,
                                            ),
                                          ),
                                          SizedBox(
                                            height: 20,
                                          ),
                                          Text(
                                            'Rs. ' +
                                                collectedOrdersList[index]
                                                            ['order']
                                                        ['total_amount']
                                                    .toString(),
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                color: Colors.grey),
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            );
                          }),
                  //!--------------Del
                  deliveredOrdersList.isEmpty
                      ? Center(
                          child: Text(
                          'No Delivered Orders',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                              fontWeight: FontWeight.bold),
                        ))
                      : ListView.builder(
                          itemCount: deliveredOrdersList.length,
                          itemBuilder: (context, index) {
                            return InkWell(
                              onTap: () async {
                                storeLocation(
                                    deliveredOrdersList[index]['order']
                                            ['address_lat'] ??
                                        24.9453,
                                    deliveredOrdersList[index]['order']
                                            ['address_long'] ??
                                        64.2342);
                                SharedPreferences preferences =
                                    await SharedPreferences.getInstance();
                                preferences.setString(
                                    'technosisOrderNumber',
                                    deliveredOrdersList[index]
                                        ['technosys_order_no']);
                                storeOrderStatus(deliveredOrdersList[index]
                                    ['delivery_status']);
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) =>
                                        //!------__---------__----
                                        TravelToDeliveryAddress(LatLng(
                                            deliveredOrdersList[index]['order']
                                                ['address_lat'],
                                            deliveredOrdersList[index]['order']
                                                ['address_long']))));
                              },
                              child: Container(
                                height: 100,
                                margin: EdgeInsets.symmetric(
                                    vertical: 3, horizontal: 5.0),
                                decoration: BoxDecoration(
                                    color: appTheme.primaryColor,
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(10.0),
                                    ),
                                    boxShadow: [
                                      BoxShadow(
                                        color: appTheme.primaryColor,
                                        blurRadius: 10.0,
                                        spreadRadius: 0.0,
                                        offset: Offset(0.0, 5.0),
                                      ),
                                    ]),
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 8.0, vertical: 8),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Container(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                .4,
                                            child: Text(
                                              deliveredOrdersList[index]
                                                  ['technosys_order_no'],
                                              maxLines: 3,
                                              style: kCollectionItemName,
                                            ),
                                          ),
                                          Text(
                                            deliveredOrdersList[index]
                                                        ['order'] ==
                                                    null
                                                ? ''
                                                : deliveredOrdersList[index]
                                                    ['order']['phone'],
                                            style: kCollectionItemPrice,
                                          ),
                                        ],
                                      ),
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Container(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                .4,
                                            child: Text(
                                              deliveredOrdersList[index]
                                                          ['order'] ==
                                                      null
                                                  ? ''
                                                  : deliveredOrdersList[index]
                                                      ['created_at'],
                                              maxLines: 3,
                                              style: kCollectionItemName,
                                            ),
                                          ),
                                          SizedBox(
                                            height: 20,
                                          ),
                                          Text(
                                            deliveredOrdersList[index]
                                                        ['order'] ==
                                                    null
                                                ? ''
                                                : 'Rs. ' +
                                                    deliveredOrdersList[index]
                                                            ['order']
                                                        ['total_amount'],
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                color: Colors.grey),
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            );
                          }),
                ]),
              )
            ],
          ),
        ),
      ),
    );
  }

  _confirmPayment(String content) async {
    var baseDialog = BaseAlertDialog(
      title: "Order Status",
      content: "$content",
      // yesOnPressed: () {
      //   Navigator.popAndPushNamed(context, '/OrdersScreen');
      // },
      // noOnPressed: () {
      //   setState(() {
      //     paymentStatus = false;
      //   });
      //   // Navigator.popAndPushNamed(context, '/CheckoutScreen');
      // },
      // yes: "OK",
      // no: "No"
    );
    await Future.delayed(Duration(milliseconds: 50));
    // Future.delayed(Duration(seconds: 2), () {
    //   Navigator.of(context).pop(true);
    // });

    showDialog(
        context: context,
        // barrierDismissible: false,
        // color: appTheme.primaryColor
        // barrierColor: appTheme.primaryColor,
        builder: (BuildContext context) => baseDialog);
  }

  //-------------riderDetailsAPI------------------------------------------------
  Future<RiderAssignedOrders> fetchRiderAssignedOrdersAPI(
      String token, String riderId) async {
    print('THIS IS FETCH RIDER ASSIGNED ORDER API METHOD');
    final response = await http.get(
      baseUrl.r_url + "rider/$riderId/assign-orders",
      headers: <String, String>{
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json'
      },
    );

    if (response.statusCode == 200) {
      var parsedJson = json.decode(response.body);
      var success = RiderAssignedOrders.fromJson(parsedJson);
      // _confirmPayment("sassssssd");
      // print('THIS IS RESPONSE ${response.body}');
      final jsonBody = jsonDecode(response.body);
      // filterOrderList(data);
      // print('this is jsonBody run type ${jsonBody['data'][0]}');
      jsonBody['data'].forEach((element) {
        print('this is element $element');
        if (element['delivery_status'] == 'delivered') {
          print('in');
          setState(() {
            deliveredOrdersList.add(element);
          });
        } else {
          setState(() {
            collectedOrdersList.add(element);
          });
          print('out');
        }
      });
      return RiderAssignedOrders.fromJson(jsonDecode(response.body));
    } else {
      print(response.statusCode.toString());
      print(response.body.toString());
      throw Exception('Failed to load Rider Details');
    }
  }

  storeOrderStatus(deliveryStatus) async {
    print('this is testing perameter $deliveryStatus');
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString('orderDeliveredStatus',
        deliveryStatus == null ? 'In process' : deliveryStatus);
    print('PUT IN STORAGE');
    print(preferences.getString('orderDeliveredStatus'));
  }

  storeLocation(lat, long) async {
    print('this is testing perameter $lat && $long');
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setDouble('addressLat', lat);
    preferences.setDouble('addressLong', long);
    print('************ ************');
  }
}
