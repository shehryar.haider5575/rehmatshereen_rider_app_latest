import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:localstorage/localstorage.dart';
import 'package:rehmatshereen_rider_app/Common/Theme.dart';
import 'package:rehmatshereen_rider_app/model/model_RiderAssignedOrders.dart';
import 'package:rehmatshereen_rider_app/screens/travel_to_delivery_address_screen.dart';
import 'package:rehmatshereen_rider_app/utils/baseurl.dart';
import 'package:shared_preferences/shared_preferences.dart';

class OrderHistory extends StatefulWidget {
  @override
  _OrderHistoryState createState() => _OrderHistoryState();
}

class _OrderHistoryState extends State<OrderHistory> {
  Future<RiderAssignedOrders> futureRiderAssignedOrders;

  BaseUrl baseUrl = BaseUrl();
  String token;
  String id, orderNo, riderId;
  final LocalStorage storage = LocalStorage('token');

  List<dynamic> deliveredOrdersList = [];

  @override
  void initState() {
    super.initState();
    token = storage.getItem("token").toString();
    riderId = storage.getItem("rider_id");
    futureRiderAssignedOrders = fetchRiderAssignedOrdersAPI(token, riderId);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        leading: InkWell(
          onTap: () {
            Navigator.of(context).pop();
          },
          child: Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
        ),
        title: Text("Order History"),
      ),
      body: Column(
        children: [
          const SizedBox(
            height: 6,
          ),
          // FutureBuilder<RiderAssignedOrders>(
          //   future: futureRiderAssignedOrders,
          //   builder: (context, snapshot) {
          //     if (snapshot.hasData) {
          //       if (snapshot.data.data.isEmpty) {
          //         return Container(
          //           color: Colors.transparent,
          //           child: Center(
          //             child: Text(
          //               'No Orders',
          //               style: TextStyle(
          //                   color: Colors.white,
          //                   fontWeight: FontWeight.bold,
          //                   fontSize: 18.0),
          //             ),
          //           ),
          //         );
          //       }
          //       return Expanded(
          //         child: ListView.builder(
          //           itemCount: snapshot.data.data.length,
          //           itemBuilder: (BuildContext context, int index) {
          //             return InkWell(
          //               onTap: () async {
          //                 storeLocation(
          //                     snapshot.data.data[index].order.addressLat ??
          //                         24.9453,
          //                     snapshot.data.data[index].order.addressLong ??
          //                         64.2342);
          //                 SharedPreferences preferences =
          //                     await SharedPreferences.getInstance();
          //                 preferences.setString('technosisOrderNumber',
          //                     snapshot.data.data[index].technosysOrderNo);
          //                 storeOrderStatus(
          //                     snapshot.data.data[index].deliveryStatus);
          //                 Navigator.of(context).push(MaterialPageRoute(
          //                     builder: (context) => TravelToDeliveryAddress()));
          //               },
          //               child: Container(
          //                 height: 100,
          //                 margin: EdgeInsets.symmetric(
          //                     vertical: 3, horizontal: 5.0),
          //                 decoration: BoxDecoration(
          //                     color: appTheme.primaryColor,
          //                     borderRadius: BorderRadius.all(
          //                       Radius.circular(10.0),
          //                     ),
          //                     boxShadow: [
          //                       BoxShadow(
          //                         color: appTheme.primaryColor,
          //                         blurRadius: 10.0,
          //                         spreadRadius: 0.0,
          //                         offset: Offset(0.0, 5.0),
          //                       ),
          //                     ]),
          //                 child: Padding(
          //                   padding: const EdgeInsets.symmetric(
          //                       horizontal: 8.0, vertical: 8),
          //                   child: Row(
          //                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //                     children: <Widget>[
          //                       Column(
          //                         mainAxisAlignment:
          //                             MainAxisAlignment.spaceEvenly,
          //                         crossAxisAlignment: CrossAxisAlignment.start,
          //                         children: <Widget>[
          //                           Container(
          //                             width: MediaQuery.of(context).size.width *
          //                                 .4,
          //                             child: Text(
          //                               // snapshot.data.data[index].order
          //                               //         .technosysOrderNo ??
          //                               snapshot.data.data[index].order == null
          //                                   ? ''
          //                                   : snapshot.data.data[index]
          //                                       .technosysOrderNo,
          //                               maxLines: 3,
          //                               style: kCollectionItemName,
          //                             ),
          //                           ),
          //                           Text(
          //                             snapshot.data.data[index].order == null
          //                                 ? ''
          //                                 : snapshot
          //                                     .data.data[index].order.phone,
          //                             // 'Phone ${snapshot.data.data[index].order.phone}',
          //                             style: kCollectionItemPrice,
          //                           ),
          //                         ],
          //                       ),
          //                       Column(
          //                         mainAxisAlignment: MainAxisAlignment.center,
          //                         children: [
          //                           Container(
          //                             width: MediaQuery.of(context).size.width *
          //                                 .4,
          //                             child: Text(
          //                               // snapshot.data.data[index].order
          //                               //         .technosysOrderNo ??
          //                               snapshot.data.data[index].order == null
          //                                   ? ''
          //                                   : snapshot
          //                                       .data.data[index].createdAt,
          //                               maxLines: 3,
          //                               style: kCollectionItemName,
          //                             ),
          //                           ),
          //                           SizedBox(
          //                             height: 20,
          //                           ),
          //                           Text(
          //                             snapshot.data.data[index].order == null
          //                                 ? ''
          //                                 : 'Rs. ' +
          //                                     snapshot.data.data[index].order
          //                                         .totalAmount,
          //                             style: TextStyle(
          //                                 fontWeight: FontWeight.bold,
          //                                 color: Colors.grey),
          //                           ),
          //                         ],
          //                       )
          //                     ],
          //                   ),
          //                 ),
          //               ),
          //             );
          //           },
          //         ),
          //       );
          //     } else if (snapshot.hasError) {
          //       return Padding(
          //         padding: const EdgeInsets.symmetric(horizontal: 15.0),
          //         child: Center(
          //           child: Text(
          //             "${snapshot.error}",
          //             textAlign: TextAlign.center,
          //           ),
          //         ),
          //       );
          //     }
          //     // By default, show a loading spinner.
          //     return Padding(
          //       padding: const EdgeInsets.only(top: 15.0),
          //       child: Center(
          //           child: CircularProgressIndicator(
          //         color: Colors.white,
          //       )),
          //     );
          //   },
          // ),
          deliveredOrdersList.isEmpty
              ? Padding(
                  padding: const EdgeInsets.only(top: 15.0),
                  child: Center(
                      child: CircularProgressIndicator(
                    color: Colors.white,
                  )),
                )
              : Expanded(
                  child: ListView.builder(
                      itemCount: deliveredOrdersList.length,
                      itemBuilder: (context, index) {
                        return InkWell(
                          onTap: () async {
                            storeLocation(
                                deliveredOrdersList[index]['order']
                                        ['address_lat'] ??
                                    24.9453,
                                deliveredOrdersList[index]['order']
                                        ['address_long'] ??
                                    64.2342);
                            SharedPreferences preferences =
                                await SharedPreferences.getInstance();
                            preferences.setString(
                                'technosisOrderNumber',
                                deliveredOrdersList[index]
                                    ['technosys_order_no']);
                            storeOrderStatus(
                                deliveredOrdersList[index]['delivery_status']);
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => TravelToDeliveryAddress(
                                    LatLng(
                                        deliveredOrdersList[index]['order']
                                            ['address_lat'],
                                        deliveredOrdersList[index]['order']
                                            ['address_long']))));
                          },
                          child: Container(
                            height: 100,
                            margin: EdgeInsets.symmetric(
                                vertical: 3, horizontal: 5.0),
                            decoration: BoxDecoration(
                                color: appTheme.primaryColor,
                                borderRadius: BorderRadius.all(
                                  Radius.circular(10.0),
                                ),
                                boxShadow: [
                                  BoxShadow(
                                    color: appTheme.primaryColor,
                                    blurRadius: 10.0,
                                    spreadRadius: 0.0,
                                    offset: Offset(0.0, 5.0),
                                  ),
                                ]),
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 8.0, vertical: 8),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      UnconstrainedBox(
                                        child: Container(
                                            padding: EdgeInsets.all(2.0),
                                            color: Colors.red,
                                            child: Text(
                                                deliveredOrdersList[index]
                                                    ['delivery_status'])),
                                      ),
                                      Container(
                                        width:
                                            MediaQuery.of(context).size.width *
                                                .4,
                                        child: Text(
                                          deliveredOrdersList[index]
                                              ['technosys_order_no'],
                                          maxLines: 3,
                                          style: kCollectionItemName,
                                        ),
                                      ),
                                      Text(
                                        deliveredOrdersList[index]['order'] ==
                                                null
                                            ? ''
                                            : deliveredOrdersList[index]
                                                ['order']['phone'],
                                        style: kCollectionItemPrice,
                                      ),
                                    ],
                                  ),
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Container(
                                        width:
                                            MediaQuery.of(context).size.width *
                                                .4,
                                        child: Text(
                                          deliveredOrdersList[index]['order'] ==
                                                  null
                                              ? ''
                                              : deliveredOrdersList[index]
                                                  ['created_at'],
                                          maxLines: 3,
                                          style: kCollectionItemName,
                                        ),
                                      ),
                                      SizedBox(
                                        height: 20,
                                      ),
                                      Text(
                                        deliveredOrdersList[index]['order'] ==
                                                null
                                            ? ''
                                            : 'Rs. ' +
                                                deliveredOrdersList[index]
                                                    ['order']['total_amount'],
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: Colors.grey),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ),
                        );
                      }),
                ),
        ],
      ),
    );
  }

  Future<RiderAssignedOrders> fetchRiderAssignedOrdersAPI(
      String token, String riderId) async {
    print('THIS IS FETCH RIDER ASSIGNED ORDER API METHOD');
    final response = await http.get(
      baseUrl.r_url + "rider/$riderId/assign-orders",
      headers: <String, String>{
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json'
      },
    );

    if (response.statusCode == 200) {
      var parsedJson = json.decode(response.body);
      var success = RiderAssignedOrders.fromJson(parsedJson);
      // _confirmPayment("sassssssd");
      // print('THIS IS RESPONSE ${response.body}');
      final jsonBody = jsonDecode(response.body);
      // filterOrderList(data);
      // print('this is jsonBody run type ${jsonBody['data'][0]}');
      jsonBody['data'].forEach((element) {
        print('this is element $element');
        if (element['delivery_status'] == 'delivered') {
          print('in');
          setState(() {
            deliveredOrdersList.add(element);
          });
        }
      });
      return RiderAssignedOrders.fromJson(jsonDecode(response.body));
    } else {
      print(response.statusCode.toString());
      print(response.body.toString());
      throw Exception('Failed to load Rider Details');
    }
  }

  filterOrderList(Data data) {
    setState(() {
      if (data.deliveryStatus == 'delivered') {
        deliveredOrdersList.add(data);
      }
    });
  }

  storeOrderStatus(deliveryStatus) async {
    print('this is testing perameter $deliveryStatus');
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setString('orderDeliveredStatus',
        deliveryStatus == null ? 'In process' : deliveryStatus);
    print('PUT IN STORAGE');
    print(preferences.getString('orderDeliveredStatus'));
  }

  storeLocation(lat, long) async {
    print('this is testing perameter $lat && $long');
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.setDouble('addressLat', lat);
    preferences.setDouble('addressLong', long);
    print('************ ************');
  }
}
