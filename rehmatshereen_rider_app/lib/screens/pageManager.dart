// import 'package:flutter/material.dart';
// import 'package:just_audio/just_audio.dart';
//
// class PageManager {
//   static const url =
//       'https://www.soundhelix.com/examples/mp3/SoundHelix-Song-2.mp3';
//   AudioPlayer _audioPlayer;
//   PageManager() {
//     _init();
//   }
//   void _init() async {
//     _audioPlayer = AudioPlayer();
//     await _audioPlayer.setUrl(url);
//   }
//
//   void play() {
//     _audioPlayer.play();
//   }
//
//   void pause() {
//     _audioPlayer.pause();
//   }
//
//   void dispose() {
//     _audioPlayer.dispose();
//   }
//
//   final progressNotifier = ValueNotifier<ProgressBarState>(
//     ProgressBarState(
//       current: Duration.zero,
//       buffered: Duration.zero,
//       total: Duration.zero,
//     ),
//   );
//   final buttonNotifier = ValueNotifier<ButtonState>(ButtonState.paused);
// }
//
// class ProgressBarState {
//   ProgressBarState({
//     this.current,
//     this.buffered,
//     this.total,
//   });
//   final Duration current;
//   final Duration buffered;
//   final Duration total;
// }
//
// enum ButtonState { paused, playing, loading }
