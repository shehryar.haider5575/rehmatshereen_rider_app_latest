import 'dart:convert';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:localstorage/localstorage.dart';
import 'package:rehmatshereen_rider_app/Common/Theme.dart';
import 'package:rehmatshereen_rider_app/Components/ImageAsAsset.dart';
import 'package:rehmatshereen_rider_app/Components/Login_and_Register/ButtonLoginRegister.dart';
import 'package:rehmatshereen_rider_app/model/model_RiderDetails.dart';
import 'package:rehmatshereen_rider_app/model/model_RiderLogin.dart';
import 'package:rehmatshereen_rider_app/utils/baseurl.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final phonecontroller = TextEditingController();
  final passwordcontroller = TextEditingController();
  final LocalStorage storage = new LocalStorage('token');
  String phone, password;
  GlobalKey<FormState> _key = new GlobalKey();
  bool visible = false;
  bool _validate = false;
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  BaseUrl baseUrl = BaseUrl();
  Future<RiderLogin> _futureRiderRegister;
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  String _deviceTokenFirebase = "Waiting for token...";

  _createNewAccount() {
    Navigator.pushNamed(context, '/register');
  }

  _loginButtonPressed() {
    Navigator.pushNamed(context, '/pageController');
  }

  loadProgress(bool value) {
    if (visible == value) {
      setState(() {
        visible = value;
      });
    } else {
      setState(() {
        visible = value;
      });
    }
  }

  setStringValuesSF(String key, String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(key, value);
    prefs.commit();
    print("getStringValuesSF: $value");
    //  prefs.setString('stringValue', "abc");
  }

  void _showErrorDialog(String error, String string) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
            "$error",
            textAlign: TextAlign.center,
          ),
        );
      },
    );

    loadProgress(false);
  }

  @override
  void initState() {
    super.initState();
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("onMessage: $message");
        String string = message['notification']['body'];
        _showDialog(string.toString(), true);
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
        String string = message['notification']['body'];
        _showDialog(string.toString(), true);
//        _navigateToItemDetail(message);
//          myBackgroundMessageHandler(message);
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
        String string = message['notification']['body'];
        print("onResume-----------------------");
        String data = message['data']['data'];

        _showDialog(data.toString(), true);
//          _navigateToItemDetail(message);
//          myBackgroundMessageHandler(message);
      },
    );
    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(
            sound: true, badge: true, alert: true, provisional: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
    _firebaseMessaging.getToken().then((String token) {
      assert(token != null);
      setState(() {
        _deviceTokenFirebase = "$token";
      });
      print(_deviceTokenFirebase);
    });
  }

  void _showDialog(String contentstring, bool value) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        if (value == false) {
          print(value);
          Future.delayed(Duration(seconds: 1), () {
            Navigator.of(context).pop(true);
          });
        } else {}
        return AlertDialog(
          elevation: 5,
          content: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: Text(
                  "$contentstring",
                  textAlign: TextAlign.left,
                  maxLines: 4,
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                ),
              )
            ],
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
            image: AssetImage(
              'Assets/Images/Login_bg.png',
            ),
            fit: BoxFit.cover),
      ),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        //SafeArea remove
        body: SingleChildScrollView(
          child: Form(
            key: _key,
            autovalidate: _validate,
            child: Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  ImageAsAsset(
                    image: 'Rehmat_e_Shereen_Logo.png',
                    height: 110.0,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 0, horizontal: 40),
                    child: Column(
                      children: <Widget>[
                        TextFormField(
                          controller: phonecontroller,
                          // validator: validatecontactNumber,
                          keyboardType: TextInputType.number,
                          // maxLength: 11,
                          onSaved: (String val) {
                            phone = val;
                          },
                          decoration: InputDecoration(
                            prefixIcon: Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10.0),
                              child: Image.asset(
                                'Assets/Images/email_icon.png',
                                scale: 2.5,
                              ),
                            ),
                            hintText: 'Contact Number',
                            hintStyle: TextStyle(color: Color(0xFFFFFFFF)),
                            border: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: appTheme.primaryColor),
                                borderRadius: BorderRadius.circular(30)),
                            enabledBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: appTheme.primaryColor),
                                borderRadius: BorderRadius.circular(30)),
                            filled: true,
                            fillColor: appTheme.primaryColor.withOpacity(0.5),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        TextFormField(
                          controller: passwordcontroller,
                          validator: validatePassword,
                          onSaved: (String val) {
                            password = val;
                          },
                          decoration: InputDecoration(
                            prefixIcon: Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10.0),
                              child: Image.asset(
                                'Assets/Images/lock_icon.png',
                                scale: 2.5,
                              ),
                            ),
                            hintText: 'Password',
                            hintStyle: TextStyle(color: Color(0xFFFFFFFF)),
                            border: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: appTheme.primaryColor),
                                borderRadius: BorderRadius.circular(30)),
                            enabledBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: appTheme.primaryColor),
                                borderRadius: BorderRadius.circular(30)),
                            filled: true,
                            fillColor: appTheme.primaryColor.withOpacity(0.5),
                          ),
                        ),
                        Align(
                          alignment: Alignment.bottomRight,
                          child: FlatButton(
                              textColor: Colors.white,
                              onPressed: () {
                                print('Forgot pass pressed');
                              },
                              child: Text('Forgot Password?')),
                        ),
                        Center(
                          child: Visibility(
                              visible: visible,
                              child: CircularProgressIndicator()),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 50),
                    child: ButtonLoginRegister(
                      buttonColor: appTheme.accentColor,
                      onPressed: () {
                        print(phonecontroller.text.toString());
                        print(passwordcontroller.text.toString());
                        _sendToServer();
                      },
                      buttonText: 'Login',
                    ),
                  ),
                  // Padding(
                  //   padding: const EdgeInsets.symmetric(horizontal: 100),
                  //   child: Container(
                  //     decoration: BoxDecoration(
                  //       border: Border(
                  //         bottom: BorderSide(
                  //           color: Colors.white,
                  //           width: 2,
                  //         ),
                  //       ),
                  //     ),
                  //     child: ButtonTransparent(
                  //       onPressed: () {
                  //         _createNewAccount();
                  //       },
                  //       textStyle: TextStyle(color: Colors.white),
                  //       buttonText: 'Create New Account',
                  //       child: null,
                  //     ),
                  //   ),
                  // ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  //------riderRegisterAPI-------------------------------------------
  Future<RiderLogin> riderLoginAPI(
      String phoneNumber, String password, String deviceToken) async {
    final http.Response response = await http.post(
      baseUrl.r_url + "rider/login",
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'contact': phoneNumber,
        'password': password,
        'device_token': deviceToken,
      }),
    );
    if (response.statusCode == 200) {
      // print('********STATUS CODE*********');
      // print(response.statusCode.toString());
      // print('********STATUS CODE*********');
      // print(response.body.toString());
      var parsedJson = json.decode(response.body);
      var success = RiderLogin.fromJson(parsedJson);
      setStringValuesSF('token', success.success.token.toString());
      storage.setItem("token", success.success.token.toString());
      fetchRiderDetailsAPI(success.success.token.toString());
      return RiderLogin.fromJson(json.decode(response.body));
    } else {
      // print(response.statusCode.toString());
      // print(response.body.toString());
      _showDialog('Invalid password', false);
      loadProgress(false);
      throw Exception('Failed to Login the Rider');
    }
  }

  //-------------riderDetailsAPI------------------------------------------------
  Future<RiderDetails> fetchRiderDetailsAPI(String token) async {
    print('this is rider details');
    final response = await http.get(
      baseUrl.r_url + "rider/details",
      headers: <String, String>{
        // 'Accept': 'application/json',
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json'
      },
    );

    if (response.statusCode == 200) {
      print(response.statusCode.toString());
      print(response.body.toString());
      var parsedJson = json.decode(response.body);
      var success = RiderDetails.fromJson(parsedJson);

      storage.setItem("id", success.success.id.toString());
      storage.setItem("rider_id", success.success.riderId.toString());
      storage.setItem("order_no", success.success.toString());
      storage.setItem("name", success.success.name.toString());
      storage.setItem("contact", success.success.contact.toString());
      storage.setItem("status", success.success.status.toString());
      print(success.success.id.toString());
      print(success.success.riderId.toString());
      print(success.success.name.toString());

      Navigator.popAndPushNamed(context, '/OrdersScreen');
      // _showDialog('Rider Login SuccessFully', false);
      loadProgress(false);

      return RiderDetails.fromJson(jsonDecode(response.body));
    } else {
      print("----------------------404-----------------------------");
      print(response.statusCode.toString());
      print(response.body.toString());
      throw Exception('Failed to load RiderDetails');
    }
  }

  //validateMobile
  String validatecontactNumber(String value) {
    String patttern = r'(^[0-9]*$)';
    RegExp regExp = new RegExp(patttern);
    if (value.length == 0) {
      return "Mobile is Required";
    } else if (value.length != 16) {
      return "Mobile number must 10 digits";
    } else if (!regExp.hasMatch(value)) {
      return "Mobile Number must be digits";
    }
    return null;
  }

  //validatePassword
  String validatePassword(String value) {
    String pattern = r'(^[0-9]*$)';
    // Pattern pattern =
    //     r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$';
    RegExp regex = new RegExp(pattern);
    print(value);
    if (value.isEmpty) {
      return 'Please enter password';
    } else {
      if (!regex.hasMatch(value))
        return 'Enter valid password';
      else
        return null;
    }
  }

  _sendToServer() {
    if (_key.currentState.validate()) {
      // No any error in validation
      _key.currentState.save();
      loadProgress(true);
      _futureRiderRegister = riderLoginAPI(
          phonecontroller.text, passwordcontroller.text, _deviceTokenFirebase);

      // print("Phone $phone");
      // print("Password $password");
    } else {
      // validation error
      setState(() {
        _validate = true;
      });
    }
  }
}
