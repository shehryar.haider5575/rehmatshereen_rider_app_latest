import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:localstorage/localstorage.dart';
import 'package:rehmatshereen_rider_app/Components/ImageAsAsset.dart';
import 'package:rehmatshereen_rider_app/model/model_RiderDetails.dart';
import 'package:rehmatshereen_rider_app/screens/Login_screen.dart';
import 'package:rehmatshereen_rider_app/utils/baseurl.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  final LocalStorage storage = new LocalStorage('token');
  String token, tokenStorage;
  BaseUrl baseUrl = BaseUrl();
  List<RiderDetails> userDetailsData;
  List<RiderDetails> userDetailsList;
  void initState() {
    super.initState();
    checkAuthToken();
  }

  checkAuthToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String username = prefs.getString('token');
    print(storage.getItem('token'));
    print(username);
    token = getStringValuesSF("token").toString();
    print(token);
  }

  getStringValuesSF(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String stringValue = prefs.getString(key);

    print("getStringValuesSF: $stringValue");
    print('---------------------');
    print("username " + prefs.getString('token').toString());
    print('---------------------');
    if (prefs.getString('token') == null) {
      // print('TOKEN SAVED TO');
      // print(token);
      Timer(Duration(seconds: 1), () {
        Navigator.of(context)
            .pushReplacement(MaterialPageRoute(builder: (context) => Login()));
      });
      // Timer(
      //     Duration(seconds: 2),
      //     () => Navigator.of(context)
      //         .push(MaterialPageRoute(builder: (context) => MapPage())));
    } else {
      // print('ELSE');
      // print(token);
      fetchRiderDetailsAPI(prefs.getString('token').toString());
    }

    return stringValue.toString();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('Assets/Images/Login_bg.png'),
                  fit: BoxFit.cover),
            ),
            // color: Colors.white,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                child: ImageAsAsset(
                  image: 'Rehmat_e_Shereen_Logo.png',
                  height: 110.0,
                ),
                // color: Colors.white,
              ),
            ],
          ),
        ],
      ),
    );
  }

  //-------------riderDetailsAPI------------------------------------------------
  Future<RiderDetails> fetchRiderDetailsAPI(String token) async {
    final response = await http.get(
      baseUrl.r_url + "rider/details",
      headers: <String, String>{
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json'
      },
    );

    if (response.statusCode == 200) {
      print(response.statusCode.toString());
      print(response.body.toString());
      var parsedJson = json.decode(response.body);
      var success = RiderDetails.fromJson(parsedJson);

      storage.setItem("id", success.success.id.toString());
      storage.setItem("rider_id", success.success.riderId.toString());
      storage.setItem("order_no", success.success.toString());
      storage.setItem("name", success.success.name.toString());
      storage.setItem("contact", success.success.contact.toString());
      storage.setItem("status", success.success.status.toString());
      print(success.success.id.toString());
      print(success.success.riderId.toString());
      print(success.success.name.toString());
      Navigator.popAndPushNamed(context, '/OrdersScreen');

      return RiderDetails.fromJson(jsonDecode(response.body));
    } else {
      print("----------------------404-----------------------------");
      print(response.statusCode.toString());
      print(response.body.toString());

      String message = json.decode(response.body)['message'] ?? '';
      String data = json.decode(response.body)['data'] ?? '';
      _showDialog(message);
      if (message == 'The access token is invalid.') {
        Navigator.popAndPushNamed(context, '/login');
      } else {
        Navigator.popAndPushNamed(context, '/OrdersScreen');
      }
      throw Exception('Failed to load RiderDetails');
    }
  }

  void _showDialog(String contentString) {
    showDialog(
      barrierDismissible: true,
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          elevation: 5,
          content: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                "$contentString",
                textAlign: TextAlign.justify,
                // maxLines: 2,
                style: TextStyle(
                    color: Colors.orange[700], fontWeight: FontWeight.bold),
              ),
            ],
          ),
        );
      },
    );
  }
}
