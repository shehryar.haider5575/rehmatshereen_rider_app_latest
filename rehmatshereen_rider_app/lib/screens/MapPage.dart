// import 'dart:async';
// import 'dart:convert';
//
// import 'package:flutter/material.dart';
// import 'package:flutter_polyline_points/flutter_polyline_points.dart';
// import 'package:google_maps_flutter/google_maps_flutter.dart';
// import 'package:http/http.dart' as http;
// import 'package:localstorage/localstorage.dart';
// import 'package:location/location.dart';
// import 'package:rehmatshereen_rider_app/Common/BaseAlertDialog.dart';
// import 'package:rehmatshereen_rider_app/Components/Login_and_Register/ButtonLoginRegister.dart';
// // import 'package:rehmatshereen_rider_app/components/base_alert_dialog.dart';
// import 'package:rehmatshereen_rider_app/Components/pin_pill_info.dart';
// import 'package:rehmatshereen_rider_app/model/model_AssignedOrderStatusDelivered.dart';
// import 'package:rehmatshereen_rider_app/model/model_AssignedOrderStatusInProcess.dart';
// import 'package:rehmatshereen_rider_app/model/model_AssignedOrderStatusOnWay.dart';
// import 'package:rehmatshereen_rider_app/model/model_AssignedOrderStatusUpdated.dart';
// import 'package:rehmatshereen_rider_app/utils/baseurl.dart';
// // import 'package:rehmatshereen_rider_app/components/custom_button.dart';
// // import 'package:rehmatshereen_rider_app/model/assigned_order_status_delivered_model.dart';
// // import 'package:rehmatshereen_rider_app/model/assigned_order_status_in_process_model.dart';
// // import 'package:rehmatshereen_rider_app/model/assigned_order_status_on_way_model.dart';
// // import 'package:rehmatshereen_rider_app/model/assigned_order_status_updated_model.dart';
// // import 'package:rehmatshereen_rider_app/src/base_url.dart';
//
// const double CAMERA_ZOOM = 12.0;
// const double CAMERA_TILT = 80;
// const double CAMERA_BEARING = 30;
// const LatLng SOURCE_LOCATION = LatLng(42.747932, -71.167889);
// const LatLng DEST_LOCATION = LatLng(37.335685, -122.0605916);
//
// class MapPageScreen extends StatefulWidget {
//   static const String routeName = '/map_page_screen';
//   State<StatefulWidget> createState() => MapPageScreenState();
// }
//
// class MapPageScreenState extends State<MapPageScreen> {
//   Completer<GoogleMapController> _controller = Completer();
//   Set<Marker> _markers = Set<Marker>();
// // for my drawn routes on the map
//   Set<Polyline> _polylines = Set<Polyline>();
//   List<LatLng> polylineCoordinates = [];
//   PolylinePoints polylinePoints;
//   String googleAPIKey = 'AIzaSyB8LPyTTDYROup1C5Y9S7jXsAM-UKWJdyc';
// // for my custom marker pins
//   BitmapDescriptor sourceIcon;
//   BitmapDescriptor destinationIcon;
// // the user's initial location and current location
// // as it moves
//   LocationData currentLocation;
// // a reference to the destination location
//   LocationData destinationLocation;
// // wrapper around the location API
//   Location location;
//   double pinPillPosition = -100;
//   PinInformation currentlySelectedPin = PinInformation(
//       pinPath: '',
//       avatarPath: '',
//       location: LatLng(0, 0),
//       locationName: '',
//       labelColor: Colors.grey);
//   PinInformation sourcePinInfo;
//   PinInformation destinationPinInfo;
//
//   BaseUrl baseUrl = BaseUrl();
//   Future<AssignedOrderStatusUpdated> _futureAssignedOrderStatusUpdated;
//   Future<AssignedOrderStatusInProcess> _futurefetchAssignedOrderStatusInProcess;
//   Future<AssignedOrderStatusOnWay> _futurefetchAssignedOrderStatusOnWay;
//   Future<AssignedOrderStatusDelivered> _futurefetchAssignedOrderStatusDelivered;
//
//   String token, riderId, orderNo;
//   double addressLat, addressLong;
//   final LocalStorage storage = new LocalStorage('token');
//
//   @override
//   void initState() {
//     super.initState();
//     token = storage.getItem("token").toString();
//     riderId = storage.getItem("rider_id").toString();
//     orderNo = storage.getItem("technosys_order_no");
//     addressLat = double.parse(storage.getItem("address_lat").toString());
//     addressLong = double.parse(storage.getItem("address_long").toString());
//     print('this is lat $addressLat.toDouble()');
//     print('this is long $addressLong.toDouble()');
//     print('this is rider id $riderId');
//     loadProgress(false);
//     loadOrderButton(true);
//     loadInProcessButtonButton(true);
//     // create an instance of Location
//     location = new Location();
//     polylinePoints = PolylinePoints();
//
//     // subscribe to changes in the user's location
//     // by "listening" to the location's onLocationChanged event
//     // location.onLocationChanged().listen((LocationData cLoc) {
//     //   // cLoc contains the lat and long of the
//     //   // current user's position in real time,
//     //   // so we're holding on to it
//     //   currentLocation = cLoc;
//     //   updatePinOnMap();
//     //   setMapPins();
//     //   //update location is here,
//     // });
//     // set custom marker pins
//
//     setSourceAndDestinationIcons();
//
//     // set the initial location
//     setInitialLocation();
//   }
//
//   void setSourceAndDestinationIcons() async {
//     BitmapDescriptor.fromAssetImage(ImageConfiguration(devicePixelRatio: 2.0),
//             'Assets/Images/destination_map_marker.png')
//         .then((onValue) {
//       sourceIcon = onValue;
//     });
//
//     BitmapDescriptor.fromAssetImage(ImageConfiguration(devicePixelRatio: 2.0),
//             'Assets/Images/driving_pin.png')
//         .then((onValue) {
//       destinationIcon = onValue;
//     });
//   }
//
//   void setInitialLocation() async {
//     // set the initial location by pulling the user's
//     // current location from the location's getLocation()
//     currentLocation = await location.getLocation();
//
//     // hard-coded destination for this example
//     destinationLocation = LocationData.fromMap({
//       "latitude": DEST_LOCATION.latitude,
//       "longitude": DEST_LOCATION.longitude
//     });
//   }
//
//   void setMapPins() {
//     setState(() {
//       // source pin
//       _markers.add(Marker(
//           markerId: MarkerId('sourcePin'),
//           // position: LatLng(24.84503136, 67.06453370000001),
//           position: LatLng(currentLocation.latitude.toDouble(),
//               currentLocation.longitude.toDouble()),
//           icon: sourceIcon as BitmapDescriptor));
//       // destination pin
//       _markers.add(
//         Marker(
//             markerId: MarkerId('destPin'),
//             position: LatLng(addressLat.toDouble(), addressLong.toDouble()),
//             icon: destinationIcon as BitmapDescriptor),
//       );
//     });
//
//     // _confirmPayment("update location");
//   }
//
//   bool visible = false;
//
//   loadProgress(bool value) {
//     if (visible == value) {
//       setState(() {
//         visible = value;
//       });
//     } else {
//       setState(() {
//         visible = value;
//       });
//     }
//   }
//
//   bool orderButton = true;
//   loadOrderButton(bool value) {
//     if (orderButton == value) {
//       setState(() {
//         orderButton = value;
//       });
//     } else {
//       setState(() {
//         orderButton = value;
//       });
//     }
//   }
//
//   bool inProcessButton = true;
//   loadInProcessButtonButton(bool value) {
//     if (inProcessButton == value) {
//       setState(() {
//         inProcessButton = value;
//       });
//     } else {
//       setState(() {
//         inProcessButton = value;
//       });
//     }
//   }
//
//   bool onWayButtonState = true;
//   loadOnWayButtonState(bool value) {
//     if (onWayButtonState == value) {
//       setState(() {
//         onWayButtonState = value;
//       });
//     } else {
//       setState(() {
//         onWayButtonState = value;
//       });
//     }
//   }
//
//   bool deliveredButtonState = true;
//   loadDeliveredButtonState(bool value) {
//     if (deliveredButtonState == value) {
//       setState(() {
//         deliveredButtonState = value;
//       });
//     } else {
//       setState(() {
//         deliveredButtonState = value;
//       });
//     }
//   }
//
//   _confirmPayment(String content) async {
//     var baseDialog = BaseAlertDialog(
//       title: "Order Status",
//       content: "$content",
//       // yesOnPressed: () {
//       //   Navigator.popAndPushNamed(context, '/OrdersScreen');
//       // },
//       // noOnPressed: () {
//       //   setState(() {
//       //     paymentStatus = false;
//       //   });
//       //   // Navigator.popAndPushNamed(context, '/CheckoutScreen');
//       // },
//       // yes: "OK",
//       // no: "No"
//     );
//     await Future.delayed(Duration(milliseconds: 50));
//     // Future.delayed(Duration(seconds: 2), () {
//     //   Navigator.of(context).pop(true);
//     // });
//
//     showDialog(
//         context: context,
//         // barrierDismissible: false,
//         // color: appTheme.primaryColor
//         // barrierColor: appTheme.primaryColor,
//         builder: (BuildContext context) => baseDialog);
//   }
//
//   void _showDialog(String contentstring, bool value) {
//     // flutter defined function
//     showDialog(
//       context: context,
//       builder: (BuildContext context) {
//         if (value == false) {
//           print(value);
//           Future.delayed(Duration(seconds: 1), () {
//             Navigator.of(context).pop(true);
//           });
//         } else {}
//
// //            Center()
//
// //        Future.delayed(Duration(seconds: 1), () {
// //                Navigator.of(context).pop(true);
// //              });
//         // return object of type Dialog
//         return AlertDialog(
//           elevation: 5,
//           // title: Text(
//           //   "$titlestring",
//           //   // "Sign Up Successfully",
//           //   textAlign: TextAlign.center,
//           //    style: TextStyle(
//           //         color: Colors.orange[700],
//           //       ),
//           // ),
//           content: Row(
//             mainAxisAlignment: MainAxisAlignment.center,
//             crossAxisAlignment: CrossAxisAlignment.center,
//             children: [
//               // Padding(
//               //   padding: const EdgeInsets.only(right: 3),
//               //   child: CircleAvatar(
//               //     radius: 15,
//               //     backgroundColor: Colors.orange[700],
//               //     child: SizedBox(
//               //       child: Icon(
//               //         Icons.login_rounded,
//               //         //  size: 25,
//               //         color: Colors.white,
//               //         // color: Colors.orange[700],
//               //       ),
//               //     ),
//               //   ),
//               // ),
//
//               Expanded(
// //                flex: 2,
//
//                 child: Text(
//                   "$contentstring",
//                   textAlign: TextAlign.left,
//                   maxLines: 4,
//                   style: TextStyle(
//                       color: Colors.white, fontWeight: FontWeight.bold),
//
//                   // "Sign Up Successfully",
//                 ),
//               )
//             ],
//           ),
//         );
//       },
//     );
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     CameraPosition initialCameraPosition = CameraPosition(
//         zoom: CAMERA_ZOOM,
//         // tilt: CAMERA_TILT,
//         // bearing: CAMERA_BEARING,
//         target: SOURCE_LOCATION);
//     if (currentLocation != null) {
//       initialCameraPosition = CameraPosition(
//         // target: LatLng(currentLocation.latitude, currentLocation.longitude),
//         target: LatLng(addressLat.toDouble(), addressLong.toDouble()),
//         zoom: CAMERA_ZOOM,
//         // tilt: CAMERA_TILT,
//         // bearing: CAMERA_BEARING
//       );
//     }
//     return Container(
//       child: Scaffold(
//         body: SafeArea(
//           child: ListView(
//             children: <Widget>[
//               Stack(
//                 children: <Widget>[
//                   Container(
//                     // height: 350.0,
//                     height: MediaQuery.of(context).size.height * .7,
//                     // decoration: BoxDecoration(
//                     //   image: DecorationImage(
//                     //     image: AssetImage('Assets/Images/maps.jpg'),
//                     //     fit: BoxFit.cover,
//                     //   ),
//                     // ),
//                     child: GoogleMap(
//                         myLocationEnabled: true,
//                         compassEnabled: true,
//                         tiltGesturesEnabled: false,
//                         markers: _markers,
//                         polylines: _polylines,
//                         mapType: MapType.normal,
//                         initialCameraPosition: initialCameraPosition,
//                         // onTap: (LatLng loc) {
//                         //   pinPillPosition = -100;
//                         // },
//                         onMapCreated: (GoogleMapController controller) {
//                           // controller.setMapStyle(Utils.mapStyles);
//                           _controller.complete(controller);
//                           setMapPins();
//
//                           setPolylines();
//
//                           // my map has compsleted being created;
//                           // i'm ready to show the pins on the map
//                           // showPinsOnMap();
//                         }),
//                   ),
//
//                   Visibility(
//                     visible: orderButton,
//                     child: Container(
//                       // color: Colors.amber,
//                       // height: 620.0,
//                       height: MediaQuery.of(context).size.height * .8,
//                       alignment: Alignment.bottomCenter,
//                       child: Stack(
//                         alignment: Alignment.centerRight,
//                         children: <Widget>[
//                           Card(
//                             elevation: 0.0,
//                             margin: EdgeInsets.symmetric(horizontal: 15.0),
//                             shape: RoundedRectangleBorder(
//                               borderRadius: BorderRadius.all(
//                                 Radius.circular(10.0),
//                               ),
//                             ),
//                             child: Container(
//                               height: MediaQuery.of(context).size.height / 13,
//                               // color: Colors.yellow,
//                               width: MediaQuery.of(context).size.width * .5,
//                               decoration: BoxDecoration(
//                                 // color: appTheme.primaryColor
//                                 color: Colors.yellow,
//                                 borderRadius: BorderRadius.all(
//                                   Radius.circular(10.0),
//                                 ),
//                                 boxShadow: [
//                                   BoxShadow(
//                                     color: Colors.black12,
//                                     // color: Colors.yellow[50],
//                                     blurRadius: 10.0,
//                                     spreadRadius: 0.0,
//                                     offset: Offset(0.0, 5.0),
//                                   ),
//                                 ],
//                               ),
//                               padding: const EdgeInsets.symmetric(
//                                   vertical: 0.0, horizontal: 0.0),
//                               child: ButtonLoginRegister(
//                                 onPressed: () {
//                                   loadProgress(true);
//                                   loadOrderButton(false);
//                                   _futureAssignedOrderStatusUpdated =
//                                       fetchAssignedOrderStatusUpdated(
//                                           token.toString(),
//                                           riderId.toString(),
//                                           orderNo.toString(),
//                                           '1');
//
//                                   // Navigator.push(
//                                   //     context,
//                                   //     MaterialPageRoute(
//                                   //       builder: (context) => (
//                                   //         fromCart: 2,
//                                   //       ),
//                                   //     ));
//                                 },
//                                 buttonText: 'Collect Order',
//                               ),
//                             ),
//                           ),
//                         ],
//                       ),
//                     ),
//                   ),
//
//                   Visibility(
//                     // maintainSize: true,
//                     // maintainAnimation: true,
//                     // maintainState: true,
//                     visible: visible,
//                     child: Row(
//                       mainAxisAlignment: MainAxisAlignment.center,
//                       // mainAxisSize: MainAxisSize.min,
//                       // crossAxisAlignment: CrossAxisAlignment.center,
//                       children: <Widget>[
//                         // Container(
//                         //   // color: Colors.amber,
//                         //   // height: 620.0,
//                         //   height: MediaQuery.of(context).size.height * .8,
//                         //   alignment: Alignment.bottomLeft,
//                         //   child: Stack(
//                         //     // alignment: Alignment.centerRight,
//                         //     children: <Widget>[
//                         //       Card(
//                         //         elevation: 0.0,
//                         //         margin: EdgeInsets.symmetric(horizontal: 15.0),
//                         //         shape: RoundedRectangleBorder(
//                         //           borderRadius: BorderRadius.all(
//                         //             Radius.circular(10.0),
//                         //           ),
//                         //         ),
//                         //         child: Container(
//                         //           height:
//                         //               MediaQuery.of(context).size.height / 14,
//                         //           // color: Colors.yellow,
//                         //           width:
//                         //               MediaQuery.of(context).size.width * .25,
//                         //           decoration: BoxDecoration(
//                         //             // color: appTheme.primaryColor
//                         //             color: Colors.yellow,
//                         //             borderRadius: BorderRadius.all(
//                         //               Radius.circular(10.0),
//                         //             ),
//                         //             boxShadow: [
//                         //               BoxShadow(
//                         //                 color: Colors.black12,
//                         //                 // color: Colors.yellow[50],
//                         //                 blurRadius: 10.0,
//                         //                 spreadRadius: 0.0,
//                         //                 offset: Offset(0.0, 5.0),
//                         //               ),
//                         //             ],
//                         //           ),
//                         //           padding: const EdgeInsets.symmetric(
//                         //               vertical: 0.0, horizontal: 0.0),
//                         //           child: ButtonLoginRegister(
//                         //             onPressed: () {
//                         //               _futurefetchAssignedOrderStatusInProcess =
//                         //                   fetchAssignedOrderStatusInProcess(
//                         //                       token,
//                         //                       riderId,
//                         //                       orderNo,
//                         //                       'in-process');
//                         //             },
//                         //             buttonText: 'In Process',
//                         //           ),
//                         //         ),
//                         //       ),
//                         //     ],
//                         //   ),
//                         // ),
//                         Visibility(
//                           visible: onWayButtonState,
//                           child: Container(
//                             // color: Colors.amber,
//                             // height: 620.0,
//                             height: MediaQuery.of(context).size.height * .8,
//                             alignment: Alignment.bottomCenter,
//                             child: Stack(
//                               // alignment: Alignment.centerRight,
//                               children: <Widget>[
//                                 Card(
//                                   elevation: 0.0,
//                                   margin:
//                                       EdgeInsets.symmetric(horizontal: 15.0),
//                                   shape: RoundedRectangleBorder(
//                                     borderRadius: BorderRadius.all(
//                                       Radius.circular(10.0),
//                                     ),
//                                   ),
//                                   child: Container(
//                                     height:
//                                         MediaQuery.of(context).size.height / 14,
//                                     // color: Colors.yellow,
//                                     width:
//                                         MediaQuery.of(context).size.width * .2,
//                                     decoration: BoxDecoration(
//                                       // color: appTheme.primaryColor
//                                       color: Colors.yellow,
//                                       borderRadius: BorderRadius.all(
//                                         Radius.circular(10.0),
//                                       ),
//                                       boxShadow: [
//                                         BoxShadow(
//                                           color: Colors.black12,
//                                           // color: Colors.yellow[50],
//                                           blurRadius: 10.0,
//                                           spreadRadius: 0.0,
//                                           offset: Offset(0.0, 5.0),
//                                         ),
//                                       ],
//                                     ),
//                                     padding: const EdgeInsets.symmetric(
//                                         vertical: 0.0, horizontal: 0.0),
//                                     child: ButtonLoginRegister(
//                                       onPressed: () {
//                                         _futurefetchAssignedOrderStatusOnWay =
//                                             fetchAssignedOrderStatusOnWay(
//                                                 token.toString(),
//                                                 riderId.toString(),
//                                                 orderNo.toString(),
//                                                 'on-way');
//                                       },
//                                       buttonText: 'on way',
//                                     ),
//                                   ),
//                                 ),
//                               ],
//                             ),
//                           ),
//                         ),
//
//                         Visibility(
//                           visible: deliveredButtonState,
//                           child: Container(
//                             // color: Colors.amber,
//                             // height: 620.0,
//                             height: MediaQuery.of(context).size.height * .8,
//                             alignment: Alignment.bottomRight,
//                             child: Stack(
//                               // alignment: Alignment.centerRight,
//                               children: <Widget>[
//                                 Card(
//                                   elevation: 0.0,
//                                   margin:
//                                       EdgeInsets.symmetric(horizontal: 15.0),
//                                   shape: RoundedRectangleBorder(
//                                     borderRadius: BorderRadius.all(
//                                       Radius.circular(10.0),
//                                     ),
//                                   ),
//                                   child: Container(
//                                     height:
//                                         MediaQuery.of(context).size.height / 14,
//                                     // color: Colors.yellow,
//                                     width:
//                                         MediaQuery.of(context).size.width * .3,
//                                     decoration: BoxDecoration(
//                                       // color: appTheme.primaryColor
//                                       color: Colors.yellow,
//                                       borderRadius: BorderRadius.all(
//                                         Radius.circular(10.0),
//                                       ),
//                                       boxShadow: [
//                                         BoxShadow(
//                                           color: Colors.black12,
//                                           // color: Colors.yellow[50],
//                                           blurRadius: 10.0,
//                                           spreadRadius: 0.0,
//                                           offset: Offset(0.0, 5.0),
//                                         ),
//                                       ],
//                                     ),
//                                     padding: const EdgeInsets.symmetric(
//                                         vertical: 0.0, horizontal: 0.0),
//                                     child: ButtonLoginRegister(
//                                       onPressed: () {
//                                         _futurefetchAssignedOrderStatusDelivered =
//                                             fetchAssignedOrderStatusDelivered(
//                                                 token.toString(),
//                                                 riderId.toString(),
//                                                 orderNo.toString(),
//                                                 'delivered');
//                                       },
//                                       buttonText: 'Delivered',
//                                     ),
//                                   ),
//                                 ),
//                               ],
//                             ),
//                           ),
//                         ),
//                       ],
//                     ),
//                   ),
//
//                   // Positioned(
//                   //   bottom: Alignment.bottomCenter.x,
//                   //   height: 100,
//                   //   // height: 200,
//                   //   // bottom: 600,
//                   //   child: Container(
//                   //     alignment: Alignment.bottomCenter,
//                   //     color: Colors.red,
//                   //     // height: MediaQuery.of(context).size.height,
//                   //   ),
//                   // ),
//                 ],
//               ),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
//
// //--------------------------fetchAssignedOrderStatusUpdated---------------------
//   Future<AssignedOrderStatusUpdated> fetchAssignedOrderStatusUpdated(
//     String token,
//     String riderId,
//     String orderNo,
//     String status,
//   ) async {
//     final http.Response response = await http.post(
//       Uri.parse(baseUrl.r_url + "rider/order-collect/status"),
//       headers: <String, String>{
//         // 'Accept': 'application/json',
//         'Authorization': 'Bearer ' + token,
//         'Content-Type': 'application/json'
//       },
//       body: jsonEncode(<String, String>{
//         'rider_id': riderId,
//         'orderNo': orderNo,
//         'status': status,
//       }),
//     );
//
//     if (response.statusCode == 200) {
//       print(response.statusCode.toString());
//       print(response.body.toString());
//       _confirmPayment('status updated successfully');
//
//       return AssignedOrderStatusUpdated.fromJson(jsonDecode(response.body));
//     } else {
//       print(response.statusCode.toString());
//       print(response.body.toString());
//       throw Exception('Failed to Assigned Order Status Updated');
//     }
//   }
// //--------------------------fetchAssignedOrderStatus_IN-process-----------------
//
//   Future<AssignedOrderStatusInProcess> fetchAssignedOrderStatusInProcess(
//     String token,
//     String riderId,
//     String orderNo,
//     String status,
//   ) async {
//     final http.Response response = await http.post(
//       Uri.parse(baseUrl.r_url + "rider/order/delivery-status"),
//       headers: <String, String>{
//         // 'Accept': 'application/json',
//         'Authorization': 'Bearer ' + token,
//         'Content-Type': 'application/json'
//       },
//       body: jsonEncode(<String, String>{
//         'rider_id': riderId,
//         'orderNo': orderNo,
//         'status': status,
//       }),
//     );
//
//     if (response.statusCode == 200) {
//       print(response.statusCode.toString());
//       print(response.body.toString());
//       _showDialog('Delivery InProcess', false);
//       loadInProcessButtonButton(false);
//       return AssignedOrderStatusInProcess.fromJson(jsonDecode(response.body));
//     } else {
//       print(response.statusCode.toString());
//       print(response.body.toString());
//       throw Exception('Failed to Assigned Order Status In Process');
//     }
//   }
//   //--------------------------fetchAssignedOrderStatus_on-way-------------------
//
//   Future<AssignedOrderStatusOnWay> fetchAssignedOrderStatusOnWay(
//     String token,
//     String riderId,
//     String orderNo,
//     String status,
//   ) async {
//     final http.Response response = await http.post(
//       Uri.parse(baseUrl.r_url + "rider/order/delivery-status"),
//       headers: <String, String>{
//         // 'Accept': 'application/json',
//         'Authorization': 'Bearer ' + token,
//         'Content-Type': 'application/json'
//       },
//       body: jsonEncode(<String, String>{
//         'rider_id': riderId,
//         'orderNo': orderNo,
//         'status': status,
//       }),
//     );
//
//     if (response.statusCode == 200) {
//       print(response.statusCode.toString());
//       print(response.body.toString());
//
//       _confirmPayment('Delivery status updated successfully.');
//       loadInProcessButtonButton(false);
//       loadOnWayButtonState(false);
//
//       return AssignedOrderStatusOnWay.fromJson(jsonDecode(response.body));
//     } else {
//       print(response.statusCode.toString());
//       print(response.body.toString());
//       throw Exception('Failed to Assigned Order Status OnWay');
//     }
//   }
//
//   //--------------------------fetchAssignedOrderStatus_Delivered----------------
//
//   Future<AssignedOrderStatusDelivered> fetchAssignedOrderStatusDelivered(
//     String token,
//     String riderId,
//     String orderNo,
//     String status,
//   ) async {
//     final http.Response response = await http.post(
//       Uri.parse(baseUrl.r_url + "rider/order/delivery-status"),
//       headers: <String, String>{
//         // 'Accept': 'application/json',
//         'Authorization': 'Bearer ' + token,
//         'Content-Type': 'application/json'
//       },
//       body: jsonEncode(<String, String>{
//         'rider_id': riderId,
//         'orderNo': orderNo,
//         'status': status,
//       }),
//     );
//
//     if (response.statusCode == 200) {
//       print(response.statusCode.toString());
//       print(response.body.toString());
//       _confirmPayment('Delivery is Delivered Successfully');
//       loadInProcessButtonButton(false);
//       loadDeliveredButtonState(false);
//       return AssignedOrderStatusDelivered.fromJson(jsonDecode(response.body));
//     } else {
//       print(response.statusCode.toString());
//       print(response.body.toString());
//       throw Exception('Failed to fetch Assigned Order Status Delivered');
//     }
//   }
//
//   void setPolylines() async {
//     // List<PointLatLng> result = await polylinePoints.getRouteBetweenCoordinates(
//     //   googleAPIKey,
//     //   currentLocation.latitude,
//     //   currentLocation.longitude,
//     //   addressLat,
//     //   addressLong,
//     //   // destinationLocation.latitude,
//     //   // destinationLocation.longitude
//     // );
//
//     // if (result.isNotEmpty) {
//     //   result.forEach((PointLatLng point) {
//     //     polylineCoordinates.add(LatLng(point.latitude, point.longitude));
//     //   });
//
//     setState(() {
//       _polylines.add(Polyline(
//           width: 50, // set the width of the polylines
//           polylineId: PolylineId("poly"),
//           color: Colors.red,
//           points: polylineCoordinates));
//     });
//   }
// }
//
// // Polylines() async {
// //   List<PointLatLng> result = await polylinePoints?.getRouteBetweenCoordinates(
// //       googleAPIKey,
// //       SOURCE_LOCATION.latitude,
// //       SOURCE_LOCATION.longitude,
// //       DEST_LOCATION.latitude,
// //       DEST_LOCATION.longitude);
// //   if (result.isNotEmpty) {
// //     // loop through all PointLatLng points and convert them
// //     // to a list of LatLng, required by the Polyline
// //     result.forEach((PointLatLng point) {
// //       polylineCoordinates.add(LatLng(point.latitude, point.longitude));
// //     });
// //   }
// //   setState(() {
// //     // create a Polyline instance
// //     // with an id, an RGB color and the list of LatLng pairs
// //     Polyline polyline = Polyline(
// //         polylineId: PolylineId("poly"),
// //         color: Color.fromARGB(255, 40, 122, 198),
// //         points: polylineCoordinates);
// //
// //     // add the constructed polyline as a set of points
// //     // to the polyline set, which will eventually
// //     // end up showing up on the map
// //     _polylines.add(polyline);
// //   });
// // }
//
// void updatePinOnMap() async {
//   // create a new CameraPosition instance
//   // every time the location changes, so the camera
//   // follows the pin as it moves with an animation
//   // CameraPosition cPosition = CameraPosition(
//   //   zoom: CAMERA_ZOOM,
//   //   tilt: CAMERA_TILT,
//   //   bearing: CAMERA_BEARING,
//   //   target: LatLng(currentLocation.latitude, currentLocation.longitude),
//   // );
//   // final GoogleMapController controller = await _controller.future;
//   // controller.animateCamera(CameraUpdate.newCameraPosition(cPosition));
//
//   // do this inside the setState() so Flutter gets notified
//   // that a widget update is due
//   // setState(() {
//   //   // updated position
//   //   var pinPosition =
//   //       LatLng(currentLocation.latitude, currentLocation.longitude);
//
//   //   sourcePinInfo.location = pinPosition;
//
//   //   // the trick is to remove the marker (by id)
//   //   // and add it again at the updated location
//   //   _markers.removeWhere((m) => m.markerId.value == 'sourcePin');
//   //   _markers.add(Marker(
//   //       markerId: MarkerId('sourcePin'),
//   //       onTap: () {
//   //         setState(() {
//   //           currentlySelectedPin = sourcePinInfo;
//   //           pinPillPosition = 0;
//   //         });
//   //       },
//   //       position: pinPosition, // updated position
//   //       icon: sourceIcon));
//   // });
// }
// // }
//
// class Utils {
//   static String mapStyles = '''[
//   {
//     "elementType": "geometry",
//     "stylers": [
//       {
//         "color": "#f5f5f5"
//       }
//     ]
//   },
//   {
//     "elementType": "labels.icon",
//     "stylers": [
//       {
//         "visibility": "off"
//       }
//     ]
//   },
//   {
//     "elementType": "labels.text.fill",
//     "stylers": [
//       {
//         "color": "#616161"
//       }
//     ]
//   },
//   {
//     "elementType": "labels.text.stroke",
//     "stylers": [
//       {
//         "color": "#f5f5f5"
//       }
//     ]
//   },
//   {
//     "featureType": "administrative.land_parcel",
//     "elementType": "labels.text.fill",
//     "stylers": [
//       {
//         "color": "#bdbdbd"
//       }
//     ]
//   },
//   {
//     "featureType": "poi",
//     "elementType": "geometry",
//     "stylers": [
//       {
//         "color": "#eeeeee"
//       }
//     ]
//   },
//   {
//     "featureType": "poi",
//     "elementType": "labels.text.fill",
//     "stylers": [
//       {
//         "color": "#757575"
//       }
//     ]
//   },
//   {
//     "featureType": "poi.park",
//     "elementType": "geometry",
//     "stylers": [
//       {
//         "color": "#e5e5e5"
//       }
//     ]
//   },
//   {
//     "featureType": "poi.park",
//     "elementType": "labels.text.fill",
//     "stylers": [
//       {
//         "color": "#9e9e9e"
//       }
//     ]
//   },
//   {
//     "featureType": "road",
//     "elementType": "geometry",
//     "stylers": [
//       {
//         "color": "#ffffff"
//       }
//     ]
//   },
//   {
//     "featureType": "road.arterial",
//     "elementType": "labels.text.fill",
//     "stylers": [
//       {
//         "color": "#757575"
//       }
//     ]
//   },
//   {
//     "featureType": "road.highway",
//     "elementType": "geometry",
//     "stylers": [
//       {
//         "color": "#dadada"
//       }
//     ]
//   },
//   {
//     "featureType": "road.highway",
//     "elementType": "labels.text.fill",
//     "stylers": [
//       {
//         "color": "#616161"
//       }
//     ]
//   },
//   {
//     "featureType": "road.local",
//     "elementType": "labels.text.fill",
//     "stylers": [
//       {
//         "color": "#9e9e9e"
//       }
//     ]
//   },
//   {
//     "featureType": "transit.line",
//     "elementType": "geometry",
//     "stylers": [
//       {
//         "color": "#e5e5e5"
//       }
//     ]
//   },
//   {
//     "featureType": "transit.station",
//     "elementType": "geometry",
//     "stylers": [
//       {
//         "color": "#eeeeee"
//       }
//     ]
//   },
//   {
//     "featureType": "water",
//     "elementType": "geometry",
//     "stylers": [
//       {
//         "color": "#c9c9c9"
//       }
//     ]
//   },
//   {
//     "featureType": "water",
//     "elementType": "labels.text.fill",
//     "stylers": [
//       {
//         "color": "#9e9e9e"
//       }
//     ]
//   }
// ]''';
// }
