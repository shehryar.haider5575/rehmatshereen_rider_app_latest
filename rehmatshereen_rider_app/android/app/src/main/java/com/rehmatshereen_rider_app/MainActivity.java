package com.rehmatshereen_rider_app;

import io.flutter.Log;
import io.flutter.embedding.android.FlutterActivity;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;


//public class MainActivity extends FlutterActivity {
//}


public class MainActivity extends FlutterActivity {

    private Intent forService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        GeneratedPluginRegistrant.registerWith(this);
        //getFlutterEngine().getDartExecutor().getBinaryMessenger()

        forService = new Intent(MainActivity.this,MyService.class);

        new MethodChannel(getFlutterEngine().getDartExecutor().getBinaryMessenger(),"com.rehmatshereen_rider_app")
                .setMethodCallHandler(new MethodChannel.MethodCallHandler() {
                    @Override
                    public void onMethodCall(MethodCall methodCall, MethodChannel.Result result) {
                        if(methodCall.method.equals("startService")){
                            startService();
                            result.success("Service Started");
                        }
                    }
                });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopService(forService);
    }

    private void startService(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            Log.d("startService>=26","startService>=26");
            startForegroundService(forService);
        } else {
            Log.d("startService.......","startService.........");
            startService(forService);

        }
    }



}
